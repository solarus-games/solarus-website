# Contributing

## Translations

The website is translated in English and French, and won't be translated to more languages.

To create a page:

1. Create a folder. Its name will be the slug.
2. Create an `index.md` file inside. This will be the English page.
3. Create an `index.fr.md` file inside. This will be the French page.
4. You can refer to the same resouces within that folder in the French and English pages.

If you can't translate the English page to French, just add this in the French page:

```yaml
---
redirectToDefault: true
---
```

## Games

### Adding a game

1. Create a directory in `content/games`, named with your game's title.

2. The directory must contain these files:

   - 📄 `index.md`: the English page.
   - 📄 `index.fr.md`: the French page.
   - 🏞️ `thumbnail.png`: the thumbnail image (300x200 pixels)
   - 📸 `screen*.png`: screenshots of your game (optional)

3. The `index.md` file must contain the following front matter:

```yaml
---
age: all # Public of your game. Possibilities are [all, warning, restricted]
controls: # A list of controls. Possibilities are [touch, mouse, keyboard, joypad]
  - keyboard
  - joypad
developer: Solarus Team # Name of the developer. Can also be a list.
download: '6933868' # Project ID on gitlab/github, or link to the .solarus file, or link to the game's page in a store. Optional.
downloadType: gitlab_api # A value among [solarus, gitlab_api, github_api, <host_platform>]. Optional.
excerpt: A game about blah blah blah. # Short description of the game.
genre: # A list of genres to define your game, such as
  - Action-RPG
  - Adventure
id: my_game_id # The unique id you choose for your game (e.g. the savegame id).
languages: # List of standard language codes for the game's translations, such as
  - en
  - fr
  - es
licenses: # List of licenses for the game, such as
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1 # Or more. Must be greater or equal than minimumPlayers.
minimumPlayers: 1 # Or more.
initialReleaseDate: 2023-01-31 # Release date of your game (if published) YYYY-MM-DD format. Optional.
latestUpdateDate: 2023-06-31 # Latest update of your game (if published) YYYY-MM-DD format. Optional.
screenshots: # List of screenshots. Path is relative to md file.
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x # Minimal version of Solarus to run the game, such as '1.6.x'
sourceCode: https://github.com/your/code # Link to the game's source code, if any. Optional.
thumbnail: thumbnail.png # Thumbnail used to display your game.
title: My Game's Title # Title of the game.
version: 1.2.3 # Version of the game (x.y.z)
walkthroughs: # Optional
  - type: video # Type of the walkthrough. Possibilities are ['video', 'walkthrough']
    url: https://your.walkthrough.com # Link to the walkthrough.
website: https://your.website.com #Link to the game's website, if any. Optional.
---
```

#### About `download` and `downloadType`

| `downloadType` | `download`                                                   |
| -------------- | ------------------------------------------------------------ |
| `solarus`      | URL to direct download the `.solarus` file                   |
| `gitlab_api`   | The id of your GitLab project                                |
| `github_api`   | `{project_owner}/{project_name}`                             |
| Host platform  | URL of your game's page on platform (e.g. Steam, itch.io...) |

> **Remarks:**
>
> - If your game uses Gitlab/Github _releases_ feature, you can omit `latestUpdateDate`; it will be automatically retrieved from the release.
> - We can't provide the `.solarus` to players if your game is hosted on an unsupported platform.

4. Below the front matter, you can write the game's description in Markdown.

5. The `ìndex.fr.md` file must only contain what is different from the English page, i.e. you should normally have this front matter:

```yaml
excerpt: Le French translation of le English excerpt.
```

6. Also, you should translate the game's description. Keep it empty to use the English one.

### Updating a game version

When you release a new version of your game, it won't automatically be published on the website. You need to make a **pull request** with the necessary changes.

- If you use Gitlab/Github _releases_ feature, you only need to update this field:
  - `version`
- If you don't, please update these fields:
  - `download`
  - `version`
  - `latestUpdateDate`

## Solarus packages (launcher and editor)

The information about player (i.e. launcher) and developer (i.e. quest editor) packages are retrieved from Gitlab API.

## Updating the Solarus version

To update the data related to Solarus itself (latest version, downloads, etc.), you need to edit the `hugo.yaml` file.

Just edit manually the `latestStableVersion` value. Ideally, this could be automated by the CI, but it's not yet the case.

```yaml
params:
  downloads:
    latestStableVersion: 1.6.5
```

When building the website, all the information is retrieved from Gitlab CI.

### Player and Developer Packages

In `content/download`, each folder with an `_index.md` that has the `downloadPackage` property will generate a package. The other information are needed to get the package for the specified version.

```yaml
title: Windows
downloadPackage: solarus-launcher
platform: windows
gitlabProjectId: 6933824
gitlabAssetFileRegex: "solarus-player-x64-v\\d+.\\d+.\\d+.zip"
version: latest
```

It is possible to specify the `version` value (e.g. `1.6.4`) or `latest` to use the value of `latestStableVersion` (from `hugo.yaml`).

## XML static RSS Feed

The last `n` news articles are available through the RSS Feed. To configure this number, edit the `rssLimit` value in `hugo.yaml`.

| URL          | Description                   |
| ------------ | ----------------------------- |
| `/index.xml` | List of latest news articles. |

## JSON static API

Quick recap of the API:

| URL                                         | Description                           |
| ------------------------------------------- | ------------------------------------- |
| **Global**                                  |
| `/index.json`                               | List of all pages.                    |
| **Downloads**                               |
| `/download/index.json`                      | List of all packages.                 |
| `/download/{package}/index.json`            | List of all platforms for a package.  |
| `/download/{package}/{platform}/index.json` | Latest version of a platform package. |
| **Games**                                   |
| `/games/index.json`                         | List of all games.                    |
| `/games/{game-id}/index.json`               | Details of a game.                    |
| **News**                                    |
| `/news/index.json`                          | List of all news articles.            |
| `/news/{news-slug}/index.json`              | Details of a news article.            |

### Global

#### List of all pages

Request:

```txt
/index.json
```

This is used by the search field script to get a compact list of all available pages.

### Downloads

#### List of all packages

Request:

```txt
/download/index.json
```

Result:

```json
{
  "version": "1.0.0",
  "data": {
    "solarus-launcher": "/download/solarus-launcher",
    "solarus-quest-editor": "/download/solarus-quest-editor"
  }
}
```

#### List of all platforms for a package

Request:

```txt
download/{package}/index.json
```

- `package`: The package identifier (`solarus-launcher`, `solarus-quest-editor`).

Result (example for `download/solarus-launcher/index.json`):

```json
{
  "version": "1.0.0",
  "data": {
    "linux": "/download/solarus-launcher/linux/",
    "macos": "/download/solarus-launcher/macos/",
    "windows": "/download/solarus-launcher/windows/"
  }
}
```

#### Latest version of a platform package

Request:

```txt
/download/{package}/{platform}/index.json
```

- `package`: The package identifier (`solarus-launcher`, `solarus-quest-editor`).
- `platform`: The platform identifier (`linux`, `macos`, `windows`).

Result (example for `download/solarus-launcher/windows/index.json`):

```json
{
  "version": "1.0.0",
  "data": {
    "changelogUrl": "",
    "checksum": "7c76175bd527b0a4df30f2a5933daccfda7c0e7b6849be634ab5907815e0458f",
    "checksumType": "sha256",
    "date": "2021-04-06",
    "fileName": "solarus-player-x64-v1.6.5.zip",
    "installerUrl": "https://gitlab.com/solarus-games/solarus/-/package_files/9016261/download",
    "platform": "windows",
    "size": 16867928,
    "version": "1.6.5"
  }
}
```

### Games

#### List of all games

Request:

```txt
/games/index.json
```

Result:

```json
{
  "version": "1.0.0",
  "data": [
    {
      "age": "all",
      "controls": ["keyboard", "gamepad"],
      "developer": ["Solarus Team"],
      "genre": ["Action-RPG", "Adventure"],
      "id": "children-of-solarus",
      "initialReleaseDate": "",
      "languages": ["en", "fr", "es"],
      "maximumPlayers": 1,
      "minimumPlayers": 1,
      "page": "/games/children-of-solarus/",
      "platforms": [],
      "solarusVersion": "1.6.x",
      "thumbnail": "/games/children-of-solarus/thumbnail.png",
      "title": "Children of Solarus",
      "version": ""
    }
    // Other games ...
  ]
}
```

#### Details of a game

Request:

```txt
/games/{game-id}/index.json
```

- `game-id`: The game unique identifier (the `id` value in a game's page yaml data, which should correspond to the savegame folder).

Result (example for `/games/zsdx/index.json`):

```json
{
  "version": "1.0.0",
  "data": {
    "age": "all",
    "content": "<Whole markdown content>",
    "controls": ["keyboard", "gamepad"],
    "developer": ["Solarus Team"],
    "download": "https://gitlab.com/solarus-games/games/zsdx/-/releases/v1.12.3/downloads/zsdx-v1.12.3.solarus",
    "downloadSize": 20835373,
    "excerpt": "The sequel to A Link to the Past, and the first game made with Solarus.",
    "genre": ["Action-RPG", "Adventure"],
    "id": "zsdx",
    "initialReleaseDate": "2011-12-16",
    "languages": ["en", "fr", "it", "es", "de", "zh"],
    "latestUpdateDate": "2021-04-06",
    "licenses": ["GPL v3", "CC-BY-SA 4.0", "Proprietary (Fair use)"],
    "maximumPlayers": 1,
    "minimumPlayers": 1,
    "page": "/games/the-legend-of-zelda-mystery-of-solarus-dx/",
    "platforms": [],
    "screenshots": [
      "/games/the-legend-of-zelda-mystery-of-solarus-dx/screen1.png",
      "..."
    ],
    "solarusVersion": "1.6.x",
    "sourceCode": "https://gitlab.com/solarus-games/games/zsdx",
    "thumbnail": "/games/the-legend-of-zelda-mystery-of-solarus-dx/thumbnail.png",
    "title": "The Legend of Zelda: Mystery of Solarus DX",
    "version": "1.12.3",
    "website": ""
  }
}
```

### News

#### List of all news articles

Request:

```txt
/news/index.json
```

Result:

```json
{
  "version": "1.0.0",
  "data": [
    {
      "date": "2023-06-28",
      "excerpt": "Without Google Translate",
      "permalink": "/news/2023-06-28-mercuris-chest-demo-english-version/",
      "thumbnail": "/news/2023-06-28-mercuris-chest-demo-english-version/thumbnail.png",
      "title": "The Legend of Zelda: Mercuris' Chest finally in english!"
    }
    // Other news articles...
  ]
}
```

#### Details of a news article

Request:

```txt
/news/{news-slug}/index.json
```

- `news-slug`: The news article unique slug.

Result (example for `/news/2019-08-20-solarus-on-retropie/index.json`):

```json
{
  "version": "1.0.0",
  "data": {
    "date": "2019-08-20",
    "excerpt": "Latest Solarus Engine is now officially integrated into RetroPie.",
    "permalink": "/news/2019-08-20-solarus-on-retropie/",
    "thumbnail": "/news/2019-08-20-solarus-on-retropie/cover.jpg",
    "title": "Solarus on RetroPie"
  }
}
```
