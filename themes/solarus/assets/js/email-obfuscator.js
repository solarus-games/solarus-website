'use strict';

document.addEventListener('DOMContentLoaded', function () {
  const showEmailButton = document.getElementById('show-email-button');
  if (showEmailButton) {
    showEmailButton.addEventListener('click', function (e) {
      e.preventDefault();
      const button = e.target;
      if (button.id === 'show-email-button') {
        const parent = button.parentNode;
        const elementA = document.createElement('a');
        elementA.classList.add('button');
        elementA.classList.add('secondary');
        elementA.classList.add('centered');
        elementA.classList.add('shortcode');
        elementA.innerText = CONTACT_EMAIL;
        elementA.href = 'mailto:' + CONTACT_EMAIL;
        parent.appendChild(elementA);
        button.remove();
      }
    });
  }
});
