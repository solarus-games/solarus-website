'use strict';

/**
 * This script loads iframes lazily, i.e. only when the user clicks on the play
 * button. This ensures the pages loads as fast as possible.
 * For YouTube, it ensures Google cookies and trackers are not loaded before the
 * user plays.
 */

const VideoService = Object.freeze({
  Unsupported: 'unsupported',
  YouTube: 'youtube',
  PeerTube: 'peertube',
});

// -----------------------------------------------------------------------------
// YouTube
// -----------------------------------------------------------------------------

function isYouTubeVideo(url) {
  const regex =
    /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/(watch\?v=|embed\/|v\/|.+\?v=)?([a-zA-Z0-9_-]{11})(\S*)?$/;
  return regex.test(url);
}

function getYouTubeVideoId(url) {
  const regex = /https:\/\/www.youtube.com\/watch\?v=([a-zA-Z0-9_-]{11})/;
  const match = url.match(regex);
  return match ? match[1] : null;
}

async function getYouTubeVideoThumbnailUrl(videoId, thumbnailType) {
  const urls = {
    maxresdefault: `https://i.ytimg.com/vi/${videoId}/maxresdefault.jpg`,
    hqdefault: `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`,
    mqdefault: `https://i.ytimg.com/vi/${videoId}/mqdefault.jpg`,
  };

  if (thumbnailType in urls) {
    return urls[thumbnailType];
  }
  return '';
}

let youTubeScriptsAdded = false;
function ensureYouTubeScriptsAdded() {
  if (youTubeScriptsAdded) return;

  const tag = document.createElement('script');
  tag.id = 'iframe-demo';
  tag.src = 'https://www.youtube.com/iframe_api';
  tag.async = true;
  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  youTubeScriptsAdded = true;
}

function createYouTubeVideoPlayer(video, onPlayerReady) {
  const iframe = video.elements.placeholderDiv;
  const videoId = video.data.videoId;
  const iframeId = `youtube-iframe-${videoId}`;
  iframe.id = iframeId;
  video.player = new YT.Player(iframeId, {
    height: '640',
    width: '360',
    videoId: videoId,
    playerVars: {
      playsinline: 1,
    },
    events: {
      onReady: () => {
        video.ready = true;
        onPlayerReady();
      },
    },
  });
}

// -----------------------------------------------------------------------------
// PeerTube
// -----------------------------------------------------------------------------

function isPeerTubeVideo(url) {
  const regex = /^(?:https?:\/\/)?([\w.-]+)\/(?:videos\/watch|w)\/([a-zA-Z0-9-]+)/;
  return regex.test(url);
}

function getPeerTubeVideoInstanceAndId(url) {
  const regex = /^(?:https?:\/\/)?([\w.-]+)\/(?:videos\/watch|w)\/([a-zA-Z0-9-]+)/;
  const match = url.match(regex);
  return {
    instanceUrl: match ? match[1] : null,
    videoId: match ? match[2] : null,
  };
}

function getPeerTubeVideoEmbedUrl(instanceUrl, videoId) {
  return `https://${instanceUrl}/videos/embed/${videoId}?title=0&warningTitle=0&peertubeLink=0&api=1`;
}

async function getPeerTubeVideoThumbnailUrl(instanceUrl, videoId) {
  const apiUrl = `https://${instanceUrl}/api/v1/videos/${videoId}`;
  try {
    const response = await fetch(apiUrl);
    if (response.ok) {
      const videoData = await response.json();
      const highestQualityThumbnail = videoData.previewPath;
      return highestQualityThumbnail ? `https://${instanceUrl}${highestQualityThumbnail}` : null;
    }
  } catch (error) {}
  return null;
}

let peertubeScriptsAdded = false;
function ensurePeerTubeScriptsAdded(onScriptLoaded) {
  if (peertubeScriptsAdded) return;

  const tag = document.createElement('script');
  tag.src = 'https://unpkg.com/@peertube/embed-api/build/player.min.js';
  tag.async = true;
  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  peertubeScriptsAdded = true;

  tag.onload = () => {
    onScriptLoaded();
  };
}

function createPeerTubeVideoPlayer(video, onPlayerReady) {
  const responsiveDiv = document.createElement('div');
  responsiveDiv.style.position = 'relative';
  responsiveDiv.style.paddingTop = '56.25%'; // 16:9 aspect ratio.

  const iframe = document.createElement('iframe');
  iframe.width = '100%';
  iframe.height = '100%';
  iframe.src = getPeerTubeVideoEmbedUrl(video.data.instanceUrl, video.data.videoId);
  iframe.frameBorder = '0'; // Deprecated, but still there for retro-compatibility.
  iframe.allowFullscreen = true;
  iframe.sandbox = 'allow-same-origin allow-scripts allow-popups allow-forms';
  iframe.style.position = 'absolute';
  iframe.style.inset = '0px';
  responsiveDiv.appendChild(iframe);

  video.elements.root.replaceChild(responsiveDiv, video.elements.placeholderDiv);
  hidePlaceholder(video);

  const PeerTubePlayer = window['PeerTubePlayer'];
  video.player = new PeerTubePlayer(iframe);
  video.player.ready.then(() => {
    video.ready = true;
    onPlayerReady();
  });
}

// -----------------------------------------------------------------------------
// General
// -----------------------------------------------------------------------------

function hidePlaceholder(video) {
  // Hide button and thumbnail.
  if (video.elements.playButton) {
    video.elements.playButton.remove();
  }
  if (video.elements.thumbnailImg) {
    video.elements.thumbnailImg.remove();
  }
  if (video.elements.placeholderDiv) {
    video.elements.placeholderDiv.remove();
  }
  // Show video.
  const iframe = video.elements.root.querySelector(`#youtube-iframe-${video.data.videoId}`);
  video.elements.iframe = iframe;
  if (iframe) {
    iframe.style.setProperty('display', 'block', 'important');
  }
}

function isVideoValid(video) {
  return (
    video.elements.placeholderDiv &&
    video.elements.thumbnailImg &&
    video.elements.playButton &&
    video.data.service !== VideoService.Unsupported
  );
}

function getVideoService(url) {
  if (isYouTubeVideo(url)) {
    return VideoService.YouTube;
  }
  if (isPeerTubeVideo(url)) {
    return VideoService.PeerTube;
  }

  return VideoService.Unsupported;
}

async function getVideoData(videoPlayerDiv) {
  const datasetUrl = videoPlayerDiv.dataset.url;
  const datasetYouTubeThumbnail = videoPlayerDiv.dataset.youtubethumbnail;
  const service = getVideoService(datasetUrl);
  switch (service) {
    case VideoService.YouTube: {
      const videoId = getYouTubeVideoId(datasetUrl);
      const thumbnailUrl = await getYouTubeVideoThumbnailUrl(videoId, datasetYouTubeThumbnail);
      return {
        service,
        url: datasetUrl,
        instanceUrl: null,
        videoId,
        thumbnailUrl,
      };
    }
    case VideoService.PeerTube: {
      const { instanceUrl, videoId } = getPeerTubeVideoInstanceAndId(datasetUrl);
      const thumbnailUrl = await getPeerTubeVideoThumbnailUrl(instanceUrl, videoId);
      return {
        service,
        url: datasetUrl,
        instanceUrl,
        videoId,
        thumbnailUrl,
      };
    }
    default:
      return {
        service: VideoService.Unsupported,
        url: null,
        instanceUrl: null,
        videoId: null,
        thumbnailUrl: null,
      };
  }
}

function getVideoElements(videoPlayerDiv) {
  const placeholderDiv = videoPlayerDiv.querySelector('.video-iframe-placeholder');
  const thumbnailImg = videoPlayerDiv.querySelector('img.video-thumbnail');
  const playButton = videoPlayerDiv.querySelector('button.video-player-button');

  thumbnailImg.addEventListener('error', () => {
    thumbnailImg.style.display = 'none';
  });
  thumbnailImg.addEventListener('load', () => {
    thumbnailImg.style.display = '';
  });

  return {
    root: videoPlayerDiv,
    placeholderDiv,
    thumbnailImg,
    playButton,
  };
}

async function getVideo(videoPlayerDiv) {
  const videoElements = getVideoElements(videoPlayerDiv);
  const videoData = await getVideoData(videoPlayerDiv);
  const video = {
    elements: videoElements,
    data: videoData,
    shouldAutoPlay: true,
    ready: false,
  };
  return video;
}

function initializeVideo(video) {
  video.elements.thumbnailImg.src = video.data.thumbnailUrl;

  switch (video.data.service) {
    case VideoService.YouTube:
      video.elements.playButton.addEventListener('click', () => {
        ensureYouTubeScriptsAdded();
        window.onYouTubeIframeAPIReady = () => {
          createYouTubeVideoPlayer(video, () => {
            if (video.player && video.ready && video.shouldAutoPlay) {
              video.shouldAutoPlay = false;
              video.player.playVideo();
              hidePlaceholder(video);
            }
          });
        };
      });
      break;
    case VideoService.PeerTube:
      video.elements.playButton.addEventListener('click', () => {
        ensurePeerTubeScriptsAdded(() => {
          createPeerTubeVideoPlayer(video, () => {
            if (video.player && video.ready && video.shouldAutoPlay) {
              video.shouldAutoPlay = false;
              video.player.play();
            }
          });
        });
      });
      break;
    default:
      break;
  }
}

document.addEventListener('DOMContentLoaded', async () => {
  const videos = [];

  // Get all buttons, iframes and video information.
  const videoPlayerDivs = document.querySelectorAll('div.video-player');
  for (const videoPlayerDiv of videoPlayerDivs) {
    const video = await getVideo(videoPlayerDiv);
    if (isVideoValid(video)) {
      videos.push(video);
      initializeVideo(video);
    }
  }
});
