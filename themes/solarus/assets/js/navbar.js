'use strict';

document.addEventListener('DOMContentLoaded', function () {
  const CSS_CLASS = 'menuOpen';
  const menuSections = document.querySelectorAll('a.nav-section');
  const menuCheckbox = document.getElementById('menu-checkbox');
  const bodyElement = document.getElementsByTagName('body')[0];

  // Prevent scroll on the page when the mobile menu is visible.
  window.addEventListener('load', function () {
    menuCheckbox.addEventListener('change', (event) => {
      if (event.target.checked) {
        bodyElement.classList.add(CSS_CLASS);
      } else {
        bodyElement.classList.remove(CSS_CLASS);
      }
    });
  });

  // Mobile-only: Close the navbar menu after item has been clicked.
  menuSections.forEach((menuSection) => {
    menuSection.addEventListener('click', function () {
      bodyElement.classList.remove(CSS_CLASS); // Fix bug on mobile.
      menuCheckbox.checked = false;
    });
  });
});

document.addEventListener('DOMContentLoaded', function () {
  const headerElement = document.getElementsByTagName('header')[0];
  if (headerElement.classList.contains('home-header')) {
    const NAVBAR_BG_VISIBLE_CSS_CLASS = 'navbar-bg-visible';

    const updateNavbarVisibility = () => {
      if (window.scrollY > 0) {
        headerElement.classList.add(NAVBAR_BG_VISIBLE_CSS_CLASS);
      } else {
        headerElement.classList.remove(NAVBAR_BG_VISIBLE_CSS_CLASS);
      }
    };

    // Show the navbar background when scrolled.
    window.addEventListener('scroll', updateNavbarVisibility);

    // The page may be loaded already scrolled down. If so, update now.
    updateNavbarVisibility();
  }
});
