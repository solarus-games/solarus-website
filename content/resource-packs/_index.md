---
title: Resource Packs
excerpt: Browse resource packs for Solarus.
tags: [resource, pack, resources, resource packs, sprite, tileset, script]
aliases:
  - /development/resource-packs
  - /dev/resource-packs
layout: redirect
redirectUrl: "{{ get-config-param resourcePacksURL }}"
---
