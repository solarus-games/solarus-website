---
date: '2003-02-08'
excerpt: Avec un peu de retard, les forums sont enfin à nouveau disponibles ! Nous les avons déménagés sur un compte sur free.fr afin de diminuer les...
tags:
- solarus
title: Réouverture des forums !
---

Avec un peu de retard, les forums sont enfin à nouveau disponibles ! Nous les avons déménagés sur un compte sur free.fr afin de diminuer les ressources utilisées par notre site sur le serveur. Contrairement à la dernière fois, tous les messages ont été conservés !

La nouvelle adresse des forums est <http://zeldasolarus.free.fr/forums> mais l'adresse <http://www.zsforums.fr.st> marche aussi.

[Visiter les forums](http://zeldasolarus.free.fr/forums)
