---
date: '2003-05-31'
excerpt: Nous savons que vous demandez beaucoup d'infos sur ZAP mais malheureusement, il est très difficile pour nous de vous offrir des captures d'écrans...
tags:
  - solarus
title: 'ZAP : Petit bilan'
---

Nous savons que vous demandez beaucoup d'infos sur ZAP mais malheureusement, il est très difficile pour nous de vous offrir des captures d'écrans ou d'autres infos sans vous dévoiler une bonne partie du jeu ce qui gâcherait ainsi la surprise...

Néanmoins, nous vous proposons un petit bilan de ce qu'il ne faut pas manquer sur ce jeu prévu pour cet été.

- [Images du jeu (images non définitives)](http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=scr)
- [Le dossier anniversaire de ZS](http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=anniv)
- [Les membres de l'équipe](http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=team)
- [Les performances du logiciel utilisé](http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=power)

En ce qui concerne la démo du jeu, vous n'aurez donc plus aucune info jusqu'à l'annonce de la date de sortie. Ensuite, de nombreuses choses risquent d'arriver à propos du jeu complet comme : l'utilisation de musiques entièrement nouvelles et orchestrées avec soin à la manière de The Wind Waker...
