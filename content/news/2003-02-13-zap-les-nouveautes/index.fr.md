---
date: '2003-02-13'
excerpt: Vous n'aviez pas eu depuis longtemps des nouvelles de ZAP, c'est certainement un jeu avec lequel nous ne souhaitons pas trop en dire, histoire de...
tags:
- solarus
title: 'Zap : les nouveautés'
---

Vous n'aviez pas eu depuis longtemps des nouvelles de ZAP, c'est certainement un jeu avec lequel nous ne souhaitons pas trop en dire, histoire de garantir l'effet de surprise mais bon, je vous dévoile gracieusement une photo d'écran de l'inventaire de la démo et peut-être de la version complète sauf si nous procédons à des changements majeurs pour celle-ci. Trèves de blabla, voici le screenshot rien que pour vous !

![](zap_subscreen.gif)

[Voir les autres screenshots](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=scr)
