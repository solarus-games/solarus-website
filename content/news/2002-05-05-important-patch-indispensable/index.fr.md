---
date: '2002-05-05'
excerpt: On vient de constater que malgré nos corrections apportées à Zelda Solarus au cours de la période de tests, un grave bug avait persisté dans la...
tags:
  - solarus
title: 'Important : patch indispensable !'
---

On vient de constater que malgré nos corrections apportées à Zelda Solarus au cours de la période de tests, un grave bug avait persisté dans la version "lourde" de Zelda Solarus (zsfullsetup.exe). En effet, il est impossible de mettre pause après le huitième donjon, ce qui est plutôt gênant...

La version disponible en téléchargement est désormais corrigée. Mais j'ai créé un petit patch (93 Ko seulement) pour éviter que vous ayez tout à retélécharger. Donc si vous avez téléchargé le jeu en version lourde (zsfullsetup.exe) avant la date d'aujourd'hui (5 mai), [cliquez ici pour télécharger le patch](download.php3?name=patch). Lancez ensuite le fichier, suivez les instructions à l'écran et le jeu sera réparé.

Voilà. Toutes nos excuses pour le dérangement causé.
