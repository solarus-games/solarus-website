---
date: '2003-09-20'
excerpt: Nous avons reçu plusieurs messages comme quoi le fichier d'installation de Zelda Solarus se bloquait à 83% sur certaines versions de Windows XP. A...
tags:
- solarus
title: 'Zelda : Mystery of Solarus version zip'
---

Nous avons reçu plusieurs messages comme quoi le fichier d'installation de Zelda Solarus se bloquait à 83% sur certaines versions de Windows XP. A partir de maintenant, une version du jeu au format zip est donc disponible. Le fichier zip contient directement les fichiers tels qu'ils sont installés par le programme d'installation normal.

Donc si le programme d'installation ne fonctionne pas sur votre PC, il vous suffit de télécharger cette version zip. Décompressez le fichier zip sur votre disque et vous pourrez jouer normalement.

[Télécharger Zelda : Mystery of Solarus](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=download)
