---
date: '2017-01-17'
excerpt: We created an official Gitter chat for things about Solarus. For the moment, there is only one room, the default one. Gitter is a popular chat whose...
tags:
- solarus
title: Solarus official Gitter
---

We created an official Gitter chat for things about Solarus. For the moment, there is only one room, the default one. Gitter is a popular chat whose benefit is being tighly linked to Github, and where you can write directly in markdown. If necessary, we will create more rooms for each projects.

### [Solarus official Gitter](https://gitter.im/solarus-games/Lobby)
