---
date: '2017-04-24'
excerpt: On se remet doucement de la sortie de Zelda XD2 ! Après un peu de repos bien mérité, l'activité sur Mercuris Chest reprend dans...
tags:
- solarus
title: Le grand parc
---

On se remet doucement de la sortie de Zelda XD2 ! Après un peu de repos bien mérité, l'activité sur Mercuris Chest reprend dans l'équipe.

![park](park-300x225.png)

Voici la première capture d'écran d'une nouvelle région en cours de création par l'incontournable Newlink : un grand parc où Link va pouvoir s'amuser mais aussi faire quelques mini-quêtes importantes pour la suite de son voyage
