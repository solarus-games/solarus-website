---
date: '2023-04-02'
excerpt: Une version corrective de quelques bugs de la démo.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Version corrective 0.1.1 de la démo de The Legend of Zelda: Mercuris' Chest"
---

## Beaucoup de correctifs !

Suite à la première publication de la démo de _The Legend of Zelda : Mercuris' Chest_ hier, vous avez été nombreux à jouer et à nous faire part de vos impressions. L'équipe vous remercie chaleureusement pour l'accueil que vous avez réservé au jeu.

Vous avez aussi rapporté quelques bugs qui se sont glissés dans cette première version et nous n'avons pas perdu une seconde à commencer à les corriger.

Aujourd'hui, nous vous proposons donc une nouvelle **version 0.1.1** qui corrige l'essentiel des bugs que nous avons repéré avec votre aide hier.

Cliquez ci-dessous pour accéder à la page du jeu et télécharger cette version corrective.

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Rappel : Comment jouer

1. Téléchargez [Solarus Launcher](/fr/download/#gamer-package).
2. Téléchargez [le jeu](/fr/games/the-legend-of-zelda-mercuris-chest/) (sous forme de fichier `.solarus`).
3. Ouvrez le fichier `.solarus` avec Solarus Launcher.

Nous sommes toujours heureux de voir vos réactions sur la communauté [Discord Solarus](https://discord.gg/TPWDr3HG).

## Pour continuer à nous rapporter des bugs

Accéder à la page [issues Gitlab de la démo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) (compte Gitlab obligatoire) ou directement sur [Discord](https://discord.gg/TPWDr3HG).

Comme toujours, soyez précis pour nous aider au mieux à reproduire le bug. Si vous souhaitez passer par Discord, pensez à mettre vos screenshots ou explications en spoiler si elle risquent de divulgâcher les autres joueurs.
