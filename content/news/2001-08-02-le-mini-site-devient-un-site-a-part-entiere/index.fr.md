---
date: '2001-08-02'
excerpt: 'Ouvert depuis le 4 Juillet, le mini-site dédié à Zelda : Mystery of Solarus connaùt un grand succès et à partir d''aujourd''hui il est devient un...'
tags:
- solarus
title: Le mini-site devient un site à part entière !!
---

Ouvert depuis le 4 Juillet, le mini-site dédié à Zelda : Mystery of Solarus connaît un grand succès et à partir d'aujourd'hui il est devient un site à part entière !

L'adresse courte reste la même : www.zelda-solarus.fr.st, mais le site est maintenant indépendant de [Consoles Power](http://www.consolespower.fr.st).

Cette petite surprise était en préparation depuis quelques jours, ce qui explique le manque de mises à jour. Maintenant que le site est opérationnel les mises à jour régulières vont revenir très vite.

Plusieurs changements sont déjà visibles sur le site : l'image de Link a changé, ainsi que les rubriques. La rubriques news a disparu, puisque tous les news sont déjà visibles sur la page d'accueil. De plus j'ai remplacé le lien vers Consoles Power par un petit logo en bas de chaque page.

D'autres nouveautés feront leur apparition prochainement, en particulier au niveau du design. Une version 2.0 du site n'est pas impossible :)

N'hésitez pas à échangez vos impressions sur le [Forum](http://www.zelda-solarus.com/forum.php3) qui ne demande qu'à être rempli ! Si vous êtes bloqué(e) dans la démo du jeu alors c'est la meilleure solution.
