---
date: '2009-04-27'
excerpt: Pour prolonger l'anniversaire des 7 ans de la sortie du premier Mystery of Solarus, je vous ai...
tags:
  - solarus
title: Zelda Solarus DX en vidéo !
---

Pour prolonger l'anniversaire des 7 ans de la sortie du premier [Mystery of Solarus](http://www.zelda-solarus.com/jeu-zs), je vous ai réservé une petite surprise : rien de moins une vidéo de [Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) ! Branchez vos hauts-parleurs ! :D

[![](http://www.zelda-solarus.com/images/zsdx/village_boy.png)](http://www.zelda-solarus.com/zsdx/videos/demo.html)

Cette vidéo vous donne un aperçu de quelques passages du début du jeu. Vous y verrez quelques personnages, le système de combats en action, quelques désagréments dont vous pourriez être victime, mais aussi la nouvelle version de certaines maisons... Je ne vous en dis pas plus, vous verrez bien :P

- [Voir la vidéo sur Zelda Solarus](http://www.zelda-solarus.com/zsdx/videos/demo.html)

- [Voir la vidéo sur Youtube](http://www.youtube.com/watch?v=r3e7t0QvImY)

Bref, j'espère que cela vous permettra de patienter avant la sortie de la démo jouable, qui ira jusqu'à la fin du premier donjon :).
