---
date: '2016-09-29'
excerpt: "The Solarus resource pack of Legend of Zelda: A Link to the Past is regularly updated even if I don't always annouce it."
tags:
  - solarus
title: 'Zelda: A Link to the Past resource pack updated'
---

The Solarus resource pack of _The Legend of Zelda: A Link to the Past_ is regularly updated even if I don't always annouce it. Lots of elements are still missing (most importantly, enemies!) but every update improves it step by step!

Today I just added a lot of non-playing character sprites. There are now more than 50 NPC sprites available! I believe we now have most character sprites from _The Legend of Zelda: A Link to the Past_.

- Download the latest [ALTTP resource pack](https://github.com/christopho/solarus-alttp-pack/releases)

![](alttp.png)

I use this resource pack a lot in my Solarus video tutorials. If you did not know yet, I made a brand new tutorial playlist because the old one was a bit outdated. The new tutorial playlist is updated every Saturday with two videos!

- [Solarus video tutorials](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufxwkj7IlfURcvCxaDCSecJY)

Follow me on Twitter ([@ChristophoZS](https://twitter.com/ChristophoZS)) to know about new tutorials and get more news about Solarus. Feel free to ask on the [Solarus forums](http://forum.solarus-games.org) if you have any question when updating your quest or following a tutorial!
