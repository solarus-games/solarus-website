---
date: '2016-04-01'
excerpt: 'NB: Clarification at the bottom of the article. For several years, we have been developing in great secrecy a project of unprecedented...'
tags:
  - solarus
title: Solatrix, a next generation handheld console [April's Fools]
---

**NB: Clarification at the bottom of the article.**

For several years, we have been developing in great secrecy a project of unprecedented ambition.

This is not a game, nor a game engine or even a game creation software. This time we went further.

![logo_solatrix](logo_solatrix-300x65.png)

We are proud to officially unveil the **Solatrix**, a revolutionary handheld console dedicated to games built with the Solarus engine!

![editor_screenshot](editor_screenshot-277x300.png)

The Solatrix will allow you to download games created with Solarus Quest Editor, both our team's games and the community games. It is expected to be available for the holidays, although it is unclear what year.

![console1](console1-300x225.jpg)

Light and handy, the Solatrix has a 3.5-inch backlit screen.

![console2](console2-225x300.jpg)

The Mercuris Pack will offer the console accompanied of Zelda Mercuris' Chest, our flagship vaporware.

![console3](console3-300x225.jpg)

Compatible with the most popular controllers on the market, the Solatrix promises hours of play more exciting than ever.

![console4](console4-300x225.jpg)

Every Solarus Software Team member has worked hard to give life to this project! The plans will be available soon so that you can also build your own Solatrix.

That's hours of fun and joy ahead!

### Edit

Obviously, it was an April's Fools. But it is not impossible to build. Actually, it is quite feasible. The console we use to make these pictures is [this one](http://www.thingiverse.com/thing:939901), by Rasmushauschild, so congrats to him for making such a beautiful console. It works with the [RetroPie](http://blog.petrockblock.com/retropie/) distribution, which is a distribution specialized in retro-gaming emulation. And as you may know, [Solarus works now on RetroPie](https://github.com/RetroPie/RetroPie-Setup/commit/11c6f1bb0dc5c731b34f05415bc745e9dfd21ed7)! The last missing thing is the little "Solatrix" sticker, but you can print it with the logo above. So now you can build your own Solatrix console!
