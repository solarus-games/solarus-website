---
date: '2017-02-06'
excerpt: 'Aujourd''hui on vous livre une capture d''écran d''une région dont on ne vous a pas encore beaucoup parlé : le grand lac ! &nbsp; Situé au...'
tags:
- solarus
title: Sur le lac
---

Aujourd'hui on vous livre une capture d'écran d'une région dont on ne vous a pas encore beaucoup parlé : le grand lac !

![lake](lake-300x225.png)

Situé au nord-est de la ville principale, on y trouve quelques îles reliées par des ponts. On a hâte de vous faire visiter tout ça !
