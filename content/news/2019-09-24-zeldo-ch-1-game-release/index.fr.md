---
date: '2019-09-24'
excerpt: Il y a 2 ans, ZeldoRetro avait créé le chapitre 1 du Défi de Zeldo. Il est désormais disponible publiquement !
tags:
  - games
thumbnail: cover.jpg
title: Sortie du Chapitre 1 du Défi de Zeldo
---

Il y a deux ans, en 2017, ZeldoRetro avait créé son premier jeu avec Solarus : le premier chapitre de sa série _Le Défi de Zeldo_, intitulé _La Revanche du Bingo_. Mais à l'époque, c'était seulement un petit défi destiné à son ami Adenothe, et pas destiné à être publié publiquement. Cependant, la sortie du second chapitre l'a fait reconsidérer cette décision, car tout le monde se demandait "Attendez ? Mais si ceci est le chapitre 2, où est donc le chapitre 1 ?". Le voici !

![Logo](game-logo.png)

Le jeu était bien moins ambitieux que sa suite. Il contient seulement un donjon, et pas de monde à explorer, ce qui le rend plutôt court. Il se finit en moins d'une heure.

![Screenshot 1](screen_1.png)

![Screenshot 2](screen_2.png)

Vous pouvez télécharger le jeu sur [sa page dans la Bibliothèque de Quêtes Solarus](/fr/games/defi-de-zeldo-ch-1).

{{< game-thumbnail "defi-de-zeldo-ch-1" >}}
