---
date: '2017-03-07'
excerpt: Les architectes de la grande ville de Zelda Mercuris' Chest ont pris soin de prévoir des espaces verts. Aujourd'hui Link emprunte un pont qui...
tags:
- solarus
title: Espaces verts
---

Les architectes de la grande ville de Zelda Mercuris' Chest ont pris soin de prévoir des espaces verts. Aujourd'hui Link emprunte un pont qui emjambe un petit bassin.

![town_small_bridge](town_small_bridge-300x225.png)

Petite information : la ville contiendra des lieux calmes comme des lieux très animés, avec de nombreuses mini-quêtes !
