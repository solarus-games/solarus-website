---
date: '2001-08-30'
excerpt: Salut à tous ! Le quatrième donjon est quasiment terminé ! Il ne manque plus que le Boss, la Carte et la Boussole. Le donjon est assez court,...
tags:
  - solarus
title: Trois screenshots du donjon 4
---

Salut à tous !

Le quatrième donjon est quasiment terminé ! Il ne manque plus que le Boss, la Carte et la Boussole. Le donjon est assez court, mais possède quand même quelques énigmes intéressantes...

Voici trois nouveaux screenshots :

[](solarus-ecran37.png)

[](solarus-ecran38.png)

[](solarus-ecran39.png)

Vous pouvez retrouver ces screenshots et les 34 autres dans la [galerie d'images](http://www.zelda-solarus.com/galerie.php3).

Echangez vos impressions sur le [forum](http://www.zelda-solarus.com/forum.php3) !
