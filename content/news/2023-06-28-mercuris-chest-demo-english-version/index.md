---
date: '2023-06-28'
excerpt: Without Google Translate
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "The Legend of Zelda: Mercuris' Chest finally in english!"
---

## Is this the real game?

Finally we are releasing an english version of our demo of _The Legend of Zelda: Mercuris' Chest_.

As a french team, we obviously made this demo in french first, but if playing a Zelda game in a non-english language is a heartbreak for you, we translated all the texts in this release.

Go to the game page to play the demo:

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## And the full game?

Currently we have no plans for the release of the full game, it will be nearly impossible for us to deliver the final version of Mercuris' Chest this year. But you will surely be pleased to follow the next updates of our project in the following months.

## Reminder: How to play

1. Download [Solarus Launcher](/en/download/#gamer-package).
2. Download [the game](/en/games/the-legend-of-zelda-mercuris-chest/) (as a `.solarus` file).
3. Open the `.solarus` file with Solarus Launcher.

If you find any typo, bug or softlock, please help us improve the game by reporting them in the [Solarus Discord server](https://discord.gg/TPWDr3HG).

You can also report bugs in the [Gitlab issues of the demo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) page (Gitlab account required).

As always, be specific enough to help us best to reproduce the bug. If you want to report on Discord, remember to put your screenshots or explanations in spoiler if they risk revealing things to other players.
