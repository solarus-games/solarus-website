---
date: '2023-06-28'
excerpt: Et sans Google Trad
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "The Legend of Zelda: Mercuris' Chest enfin en anglais !"
---

## Is this the real game?

Enfin, nous publions une version anglaise de notre démo de _The Legend of Zelda: Mercuris' Chest_.

Nous avons évidemment commencé à sortir le jeu en français pour tenir le delai du 1er avril, mais si jouer à un jeu Zelda dans une autre langue que l'anglais vous perturbe, nous avons traduit tous les textes pour vous.

Rendez-vous sur la page du jeu pour jouer à la démo :

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Et le jeu complet ?

Pour le moment, nous n'avons aucun projet pour la sortie du jeu complet, il sera quasiment impossible pour nous de terminer la version finale de Mercuris' Chest cette année. Mais soyez au rendez-vous pour suivre les news sur le projet dans les prochains mois.

## Rappel : Comment jouer

1. Téléchargez [Solarus Launcher](/fr/download/#gamer-package).
2. Téléchargez [le jeu](/fr/games/the-legend-of-zelda-mercuris-chest/) (sous forme de fichier `.solarus`).
3. Ouvrez le fichier `.solarus` avec Solarus Launcher.

Si vous trouvez des fautes, bugs ou que vous êtes bloqués, aidez-nous à améliorer le jeu sur le [Discord Solarus](https://discord.gg/TPWDr3HG).

Vous pouvez aussi nous rapporter tout cela sur la page [issues Gitlab de la démo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) (compte Gitlab obligatoire).

Comme toujours, soyez précis pour nous aider au mieux à reproduire le bug. Si vous souhaitez passer par Discord, pensez à mettre vos screenshots ou explications en spoiler si elle risquent de divulgâcher les autres joueurs.
