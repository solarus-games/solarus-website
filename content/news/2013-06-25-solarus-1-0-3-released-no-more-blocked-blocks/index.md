---
date: '2013-06-25'
excerpt: Due to Murphy's law, I introduced an important bug at the very end of my tests, just before releasing Solarus 1.0.2. This bug made you fail to push...
tags:
- solarus
title: Solarus 1.0.3 released, no more blocked blocks
---

Due to Murphy's law, I introduced an important bug at the very end of my tests, just before releasing Solarus 1.0.2. This bug made you fail to push blocks completely (the last pixel of the movement was not done). So here is a new patch release (1.0.3) that just fixes this bug. You really want to upgrade.

There are no changes in our games (ZSDX and ZSXD) but they get a new version number (1.6.1) because on some systems, the engine is included with the game.

- [Download Solarus 1.0.3](http://www.solarus-games.org/downloads/download-solarus/ "Download Solarus")
- [Download Zelda Mystery of Solarus DX 1.6.1](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/ "Download ZSDX")
- [Download Zelda Mystery of Solarus XD 1.6.1](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/ "Download ZSXD")

## Changes in Solarus 1.0.3

- Fix blocks not completely moved since 1.0.2.

Sorry about that and thanks for your feedback.

(I really should make unit tests one day!)
