---
date: '2011-12-31'
excerpt: Deux semaines après sa sortie, Zelda Mystery of Solarus DX a déjà été téléchargé plus de 5000 fois, un nombre en accélération car on...
tags:
  - solarus
title: 'Zelda Mystery of Solarus DX : version 1.3 conseillée'
---

Deux semaines après sa sortie, Zelda Mystery of Solarus DX a déjà été téléchargé plus de 5000 fois, un nombre en accélération car on commence à en parler de plus en plus sur Internet. Merci donc à tous pour ce qui est déjà un succès :)

Merci aussi pour vos nombreux retours en ce qui concerne les inévitables bugs et les suggestions d'améliorations. Les principaux bugs connus ont été corrigés : il y avait des cas où le jeu pouvait planter brutalement (en particulier dans les donjons 7 et 9), des cas où certains boss pouvaient se bloquer et vous forcer à relancer le jeu, ou encore une possibilité de vous faire téléporter par erreur dans une zone plutôt secrète accessible seulement vers la fin du jeu. :rolleyes:

Ces problèmes ne se produisaient pas systématiquement (c'est pour ça qu'ils nous ont échappé dans un premier temps ^\_^). En tout cas, la version 1.3 corrige (entre autres) ces soucis. Je vous recommande donc de la télécharger, même si vous n'avez pas rencontré de problème particulier pour l'instant. Vos sauvegardes sont bien sûr conservées si vous réinstallez le jeu.

- [Télécharger Zelda Mystery of Solarus DX 1.3](http://www.zelda-solarus.com/jeu-zsdx-download)

Une version 1.4 est déjà dans les cartons. Elle contient des optimisations, des corrections de bugs moins urgents et de quoi faciliter les traductions. Les mises à jour seront par la suite de moins en moins fréquentes : le jeu devenant stable, ce seront essentiellement des traductions dans d'autres langues.
