---
date: '2018-01-21'
excerpt: The Solarus Team is gonna show you the progress on our first 100% original free game, Children of Solarus, with a new screenshot each two weeks. The...
tags:
  - solarus
title: Children of Solarus (1st fortnightly screenshot)
---

The Solarus Team is gonna show you the progress on our first 100% original free game, Children of Solarus, with a new screenshot each two weeks. The mapping is still a bit experimental, so that in the final game there may be many changes, improvements and surprises with respect to what you see in our images.

**Warning! Spoilers ahead!**

This is the first screenshot, showing a secret dungeon, hidden (or not so hidden?) close to the village:

![](1.-18-01-21-300x225.png)

We have been working on improved weapons, like a new scripted shield that can be used directly (like the shield in Link's Awakening or Minish Cap). The image shows the first shield of the game, the wooden shield, carried by our legendary hero Eldran. Stay tuned for more forthcoming news!
