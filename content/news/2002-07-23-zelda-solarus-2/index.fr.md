---
date: '2002-07-23'
excerpt: Le sujet brûlant du site est de retour, l'info qu'on vous cache depuis plus d'un mois. Préparez-vous car nous vous dévoilons enfin l'existence du...
tags:
  - solarus
title: Zelda Solarus 2
---

![](mini001.gif)

Le sujet brûlant du site est de retour, l'info qu'on vous cache depuis plus d'un mois.

Préparez-vous car nous vous dévoilons enfin l'existence du second jeu de la ZSTeam : **Zelda Solarus 2 !!!** créé avec le logiciel The Games Factory.

Appeler un jeu avec un "2" à la fin, c'est pas génial alors on est en train de réfléchir à un éventuel nom en fonction du scénario comme on l'a fait pour Mystery of Solarus. Vous trouverez bientôt toutes les rubriques dédiées au jeu, screenshots, scénario, et téléchargement d'une démo technique que vous pourrez bientôt tester.

On doit quand même préciser que cette démo est loin de ressembler au jeu final puisque nous faisons même un niveau que vous ne verrez pas dans la version complète.

Pour ce qui est d'une date de sortie, aucune n'est décidée même pour la démo. On peut simplement prévoir qu'elle devrait être disponible vers la rentrée de Septembre. C'est donc à partir de cette sortie que nous allons commencer la programmation du jeu final avec ces donjons et les quêtes parallèles.

Ce n'est pas tout, nous vous signalons l'arrivée de Julien, notre scénariste qui travaille dessus depuis pas mal de temps, il fait du très bon travail la plupart du temps et c'est grâce à lui que nous pourrons créer ce jeu rapidement.

Préparez-vous donc à suivre pendant quelques mois ce développement auquel on consacrera des news de temps en temps avec l'actu Zelda générale, de nombreuses infos seront de la partie pour vous faire profiter au maximum du jeu.
