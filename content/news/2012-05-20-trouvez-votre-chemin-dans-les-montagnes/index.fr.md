---
date: '2012-05-20'
excerpt: Voici encore un nouvel épisode de la soluce vidéo de Zelda Mystery of Solarus DX.
tags:
  - solarus
title: Trouvez votre chemin dans les montagnes
---

Voici encore un nouvel épisode de la [soluce vidéo de Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-soluce).
Il va du niveau 7 au niveau 8, et au passage vous permet de trouver beaucoup de Fragments de Coeur, de rubis et autres objets.

- [Épisode 16 : du niveau 7 au niveau 8](http://www.youtube.com/watch?v=yMUZTDv7_Vs)

Merci à Sam101 pour les commentaires.
Il ne reste plus que trois épisodes avant d'atteindre la fin du jeu... On y est presque !
