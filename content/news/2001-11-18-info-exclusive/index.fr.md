---
date: '2001-11-18'
excerpt: ' Le temple des souvenirs, c''est un temple magique suspendu au dessus du vide ! Il ne demeure pas en Hyrule mais dans un autre monde... Ce...'
tags:
- solarus
title: INFO EXCLUSIVE !!!
---

![](9templ03.gif)
![](9templ02.gif)

Le temple des souvenirs, c'est un temple magique suspendu au dessus du vide ! Il ne demeure pas en Hyrule mais dans un autre monde... Ce monde il n'est accessible que par la pensée ! Je pense faire de ce temple le plus mystérieux du jeu... J'ai intitulé ce temple ainsi car ce temple n'existe que dans les pensées de Link mais aussi parce qu'il va retrouver de nombreux êtres qui lui sont chers et qui vont lui réveler certains secrets...

Composé de 4 étages non contigus, ce temple est très long et dur car il s'agit du dernier. Voici d'ailleurs quelques screenshots exclusifs très révélateurs...

News de Netgamer
