---
date: '2018-02-18'
excerpt: "Et on continue avec de nouvelles images du remake de Link's Awakening : A Link to the dream !"
tags:
  - solarus
title: La cave Flagello
---

Et on continue avec de nouvelles images du remake de Link's Awakening : A Link to the dream !
Cette semaine, c'est la cave Flagello qui est à l'honneur.  Pour ceux qui ne connaissent pas, il s'agit du premier donjon du jeu. Je suis vraiment certain que vous reconnaitrez les différents lieux présents sur ces images.

![](1-2-300x240.png)

![](2-2-300x240.png)

![](3-2-300x240.png)

![](4-2-300x240.png)
