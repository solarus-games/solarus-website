---
date: '2003-07-16'
excerpt: 'Vous ne rêvez pas, la démo de Zelda : Advanced Project est sortie ! Nous avons en effet préféré ne pas annoncer de date pour éviter d''avoir à...'
tags:
- solarus
title: 'ZAP : la démo !'
---

Vous ne rêvez pas, la démo de Zelda : Advanced Project est sortie ! Nous avons en effet préféré ne pas annoncer de date pour éviter d'avoir à nous précipiter pour la finir.

La démo fait 3.3 Mo. On estime la durée de vie à quelques heures. N'oubliez pas de lire le mode d'emploi avant de jouer. On attend vos réactions dans les commentaires de cette news ou sur le forum !

Amusez-vous bien !

[Mode d'emploi de la démo](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=notice)

[Télécharger la démo](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=download)
