---
date: '2022-12-17'
excerpt: 'Christmas present for Solarus fans: a brand new website!'
tags:
  - solarus
  - website
thumbnail: thumbnail.png
title: A brand new website for Solarus!
---

## A new website

We are happy to announce the release of a brand new website for Solarus! This is a Christmas present for Solarus fans, and we hope you will like it.

The website is now based on [Hugo](https://gohugo.io/), a static site generator. This means that the website is now much faster to load, and it is also easier to maintain. The website is still available in both English and French.

![Solarus website](website.png)

## A new logo

Solarus also has a brand new logo that you can see on the [Press Kit page](/about/press-kit/), along with enhanced brand colors.

This logo should look sleeker and more professional. We hope you will like it.

![Solarus logo](new-logo.png)

## A new documentation website

The documentation is now available on a separate website: [docs.solarus-games.org](https://docs.solarus-games.org/). This website is based on [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/), a theme for MkDocs. This means that the documentation is now much easier to read and navigate.

The Lua API is still not available on this website, but we are working on it. In the meantime, you can still access the Lua API documentation on the [old documentation website](https://doxygen.solarus-games.org/latest/).

![Solarus documentation](documentation.png)

## Merry Christmas!

We wish you a Merry Christmas and a Happy New Year! We hope you will enjoy the new website and the new documentation website.
