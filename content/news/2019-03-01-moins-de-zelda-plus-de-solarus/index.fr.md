---
date: '2019-03-01'
excerpt: 'Oyez, oyez ! Du nouveau sur Zelda Solarus ! Souvenez-vous : il y a quelques semaines...'
tags:
  - solarus
thumbnail: cover.jpg
title: Moins de Zelda, plus de Solarus
---

Oyez, oyez ! Du nouveau sur Zelda Solarus !

![teaser](teaser-300x225.jpg)

Souvenez-vous : il y a quelques semaines, nous avions publié cette **mystérieuse affiche** sans plus de détails que le simple mot "Bientôt...".

Aujourd'hui, on vous explique tout. Accrochez-vous bien, il va y avoir du changement !

## Un site moderne dédié à la création de jeux

Rappelons qu'historiquement, Zelda Solarus est consacré depuis 2001 aux deux thèmes suivants :

- le développement de nos jeux amateurs avec le moteur de jeu Solarus,
- les Zelda officiels.

Parmi ces deux volets, vous n'êtes pas sans remarquer que le contenu sur les Zelda officiels n'est plus très à jour et n'évolue plus depuis longtemps. Après tout, ce qui fait que Zelda Solarus est unique, et ce qui motive l'équipe et les Solarusiens que vous êtes, c'est d'abord la création de jeux amateurs. Que ce soit pour y jouer ou pour les créer. De plus, étant donné que nous travaillons désormais sur des projets de jeux amateurs qui ne sont plus forcément des Zelda, ces deux thèmes ne font que s'éloigner l'un de l'autre. La forme actuelle de Zelda Solarus montre donc ses limites.

C'est pour toutes ces raisons que depuis maintenant 2 ans, Binbin et Neovyse travaillent sur une nouvelle version de Zelda Solarus qui va **se recentrer sur l'essentiel : les jeux amateurs**.

Et parallèlement à Zelda Solarus, existe aussi notre blog de développement [Solarus-Games](http://www.solarus-games.org), qui lui était déjà destiné exclusivement au développement de jeux amateurs avec le moteur Solarus, mais n'existait jusqu'à présent qu'en anglais. Beaucoup de contenu se retrouvait dupliqué entre les deux sites, notamment les pages de téléchargement de nos jeux et les annonces des nouvelles versions de Solarus et Solarus Quest Editor. Difficile d'y voir clair entre ces deux sites pour qui voudrait s'initier à la création de jeux.

Vous l'avez donc sans doute déjà deviné : nous avons décidé de **fusionner nos deux sites** en un seul, dont le but sera de diffuser les jeux créés avec Solarus et d'apprendre à développer votre propre jeu.

- Le Solarus-Games actuel va subir une grande refonte pour devenir bien plus qu'un blog de développement : une véritable plate-forme moderne qui proposera de plus en plus de jeux créés avec Solarus, ainsi que des tutoriels pour débuter dans la création de jeux.
- Zelda Solarus ne traitera plus des Zelda officiels, et va se concentrer sur la création de jeux avec Solarus.

Le contenu sur les Zelda officiels étant terminé, Zelda Solarus va donc en toute logique devoir **changer de nom** pour devenir la version française de Solarus-Games.

Cette nouvelle refonte proposera de nombreuses nouveautés :

- Une présentation moderne et attrayante de Solarus et Solarus Quest Editor, un préalable indispensable, que vous soyez joueur ou créateur !
- Une bibliothèque de quêtes : la liste des jeux réalisés avec Solarus, avec la possibilité de rechercher et de filtrer selon des critères comme la langue. Ces jeux pourront être **des Zelda ou non**, et vont inclure **ceux de l'équipe mais aussi ceux de la communauté**. Chaque quête disposera de sa propre page dédiée, moderne et professionnelle, qui donnera tous les détails sur le jeu et permettra bien entendu de le télécharger.
- Une bibliothèque de packs de ressources : de la même façon que les jeux, il sera possible de **télécharger des packs de graphismes, de musiques et de scripts** qui vous serviront de base pour créer vos projets de jeux. Exemples : le pack de ressources Zelda: A Link to the Past ou encore le pack de ressources libres officiel Solarus.
- Des tutoriels écrits : **tout pour vous apprendre** à créer une quête avec Solarus si vous avez envie de vous lancer !
- Un site multilingue : initialement, Solarus-Games sera disponible **en français et en anglais**.
- Un site open source : comme le reste de Solarus, le site sera entièrement open source, afin de **faciliter les contributions** comme l'ajout de **nouveaux jeux**, de **traductions** ou des **nouveaux tutoriels** par la communauté (avec bien entendu la validation par la Solarus Team !).

Et un jour, nous aimerions que la bibliothèque de quêtes soit disponible dans l'application Solarus elle-même, afin que les joueurs puissent directement télécharger des jeux depuis Solarus pour Android ou Solarus pour PC et Mac. Mais je m'égare : cette nouveauté arrivera dans un second temps !

## Foire aux questions

### On y verra enfin plus clair dans tous vos projets de jeux amateurs ?

C'est notre but numéro 1.

### Mais à quoi va ressembler cette nouvelle version ?

Je suis ravi que vous me le demandiez enfin ! Il nous reste encore beaucoup de travail mais les premiers résultats ressemblent à ceci :

![image1](solarus_website_1-246x300.png)

![image2](solarus_website_2-300x173.png)

![image3](solarus_website_3-300x183.png)

### Que deviendront les contenus sur les Zelda officiels, comme les solutions complètes ?

Les contenus sur les Zelda officiels restent en effet valables et précieux, même s'ils manquent de mises à jour depuis trop longtemps. On pense notamment à toutes nos solutions complètes, détaillées et de qualité. Nous avons choisi de les offrir avec plaisir aux copains de [Zeldaforce](https://www.zeldaforce.net), en remerciement pour tout le travail réalisé par Binbin dans le développement de ce nouveau site Solarus-Games.

### Et le forum Zelda Solarus ?

Aucun changement concernant les forums. Le forum [Solarus-Games](http://forum.solarus-games.org) restera le lieu de discussion préférentiellement en anglais, et le forum [Zelda Solarus](http://forums.zelda-solarus.com), avec ses 15 ans d'histoire, continuera d'être le lieu de discussions en français.

### Et au fait, c'est prévu pour quand ?

Le 1er avril, bien évidemment.

## En conclusion

Solarus va disposer d'un site moderne, attrayant, ouvert et multilingues. Tout est prêt pour que la communauté s'agrandisse. C'est la fin d'un chapitre et le commencement d'une nouvelle ère, mes amis !
