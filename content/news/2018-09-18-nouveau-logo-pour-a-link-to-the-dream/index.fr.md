---
date: '2018-09-18'
excerpt: Ces derniers jours, nous avons beaucoup travaillé sur le projet de remake de Link's Awakening notamment au niveau des cinématiques. C'est vraiment...
tags:
- solarus
title: Nouveau logo pour A Link to the Dream
---

Ces derniers jours, nous avons beaucoup travaillé sur le projet de **remake de Link's Awakening** notamment au niveau des cinématiques. C'est vraiment très intense car nous utilisons les dernières avancées du moteur. Cela nous permet vraiment d'avoir vraiment un jeu de très grande qualité.

De plus, Neovyse a retravaillé le logo du jeu qui est absolument magnifique. Merci à lui !

![](logo-01-300x203.png)

N'hésitez pas à donner votre avis !
