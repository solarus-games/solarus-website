---
date: '2001-12-15'
excerpt: Salut à tous ! Le niveau 7 est en pleine construction. Ce sera un grand palais sur 3 étages, avec pas mal d'énigmes novatrices par rapport aux...
tags:
- solarus
title: Ca avance...
---

Salut à tous !

Le niveau 7 est en pleine construction. Ce sera un grand palais sur 3 étages, avec pas mal d'énigmes novatrices par rapport aux niveaux précédents. Elles devraient vous rappeler certains passages des derniers donjons de Zelda 3... Je ne vous en dis pas plus !!

L'objectif est de terminer ce donjon avant la fin du mois. En ce moment j'ai pas mal de boulot mais des que les vacances arriveront j'aurai enfin du temps.

Bientôt des screenshots de ce donjon !
