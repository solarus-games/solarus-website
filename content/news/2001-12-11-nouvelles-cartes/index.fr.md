---
date: '2001-12-11'
excerpt: Pour faire que ce jeu soit encore plus beau, on a refait les cartes et boussoles des donjons ! C'est beaucoup plus soigné ! D'ailleurs, voici deux...
tags:
- solarus
title: Nouvelles cartes
---

Pour faire que ce jeu soit encore plus beau, on a refait les cartes et boussoles des donjons ! C'est beaucoup plus soigné ! D'ailleurs, voici deux exemple, avant et après la modification...

|  |
| --- |
| AVANT
Toutes plates avec quand même les légendes et le curseur qui désigne
l'endroit où se trouve Link. |

|  |
| --- |
|  APRES
Plus beau avec un fond granulé ! L'image ci-contre est en taille
réelle ! Ce genre de graphismes a été fait avec Paint Shop Pro ! Voila
pour l'info ! |
