---
date: '2018-02-03'
excerpt: Ahoy there! The Solarus Team has a second screenshot for you, from the project Children of Solarus. This time we show some houses of the village....
tags:
- solarus
title: Children of Solarus (2nd fortnightly screenshot)
---

Ahoy there! The Solarus Team has a second screenshot for you, from the project **Children of Solarus**. This time we show some houses of the village. Yup, those annoying birds that you can see in the image are cluckos! (As you may have guessed, cluckos will play a similar role in our games as cuccos do in Zelda games, although cluckos will be cooler and more stupid.) This small village is still under construction by the team, and more details will be added eventually, including the maps inside of houses. More incoming news for you in two weeks![](2.-18-02-04-300x224.png)
