---
date: '2001-11-24'
excerpt: Bienvenue sur Zelda Solarus Nouvelle formule ! Beaucoup plus beau qu'avant et beaucoup plus rapide ! Ce nouveau site offrira un grand nombre de...
tags:
- solarus
title: On change tout !
---

Bienvenue sur Zelda Solarus Nouvelle formule !

Beaucoup plus beau qu'avant et beaucoup plus rapide ! Ce nouveau site offrira un grand nombre de services qui seront en ligne d'ici une semaine !

Toutes les options du site sont disponibles et fonctionnent correctement ! Mais si vous trouvez un bug, contactez nous vite !

Vous pouvez toujours laisser des messages sur le [forum](http://www.zelda-solarus.com/forum.php3) concernant le jeu ou le site !

Je crois que tout est clairement dit ! Je vous souhaite une agréable visite sur Zelda Solarus !
