---
date: '2001-07-09'
excerpt: "La démo avait un petit défaut technique à déplorer : on ne voyait pas l'animation de l'Epée lorsque Link combattait. RPG Maker oblige. Pour..."
tags:
  - solarus
title: "Nouveau : l'animation de l'Epée de Link !"
---

La [démo](http://www.zelda-solarus.com/demo.php3) avait un petit défaut technique à déplorer : on ne voyait pas l'animation de l'Epée lorsque Link combattait. RPG Maker oblige.

Pour solutionner ce problème, il a fallu encore repousser les limites du logiciel. Par le biais d'une simple animation de combat, j'ai réussi à donner son animation à Link, comme dans Zelda 3. En fait le principe est simple : il "suffit" d'afficher l'animation de combat correspondant à un coup d'épée à chaque fois que le joueur (vous) appuie sur Espace. C'est exactement le même système que pour afficher l'animation de combat correspondant à une explosion lorsque vous posez une Bombe (voir une des précédentes [news](http://www.zelda-solarus.com/news.php3?id=8)). Simple, mais il fallait y penser !

[](solarus-ecran24.png)

[](solarus-ecran25.png)

[](solarus-ecran26.png)

[](solarus-ecran27.png)

Comme vous le voyez, Link utilise bien son Epée pour couper les salades et surtout combattre les ennemis. Inutile de vous dire que l'intérêt des combats est complètement relancé !
