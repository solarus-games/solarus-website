---
date: '2024-02-20'
excerpt: Des images qui montrent un système jour/nuit pour encore plus d'immersion
tags:
  - games
  - mercuris
thumbnail: thumbnail.png
title: "Zelda : Mercuris' Chest se montre en images"
---

Comme la dernière fois, nous dévoilons quelques images de notre prochain fangame Zelda avec 5 nouveaux screenshots.

## Système jour/nuit

En janvier, on vous révélait que le jeu était désormais en écran large, cette fois-ci, c'est le système jour/nuit que nous vous montrons à travers quelques images du jeu. Vous pouvez donc constater que le temps passe dans ce monde et que ça aura une incidence sur les événements qui se produiront.

![Coucher de soleil sur le lac](mc_dusk.png)

![La nuit dans un village](mc_night.png)

![Le soleil se lève sur la baie](mc_dawn.png)

![Un coffre inaccessible](mc_dungeon_1.png)

![Les Gibdos sont coriaces](mc_dungeon_2.png)

Rien de révolutionnaire cependant, d'autres jeux créés avec Solarus ont un système similaire. Mais ce cycle en "temps réel" faisait partie de nos plans depuis de nombreuses années. Solarus est désormais un moteur tellement complet que développer ce genre de petite touche est plutôt accessible.

Restez à l'affût tout au long de l'année pour encore plus d'infos et d'images du jeu.
