---
date: '2017-03-22'
excerpt: La capture d'écran de la semaine concernant notre projet Mercuris' Chest vous dévoile aujourd'hui un peu plus la forêt du nord...
tags:
- solarus
title: Deux forêts, deux ambiances
---

La capture d'écran de la semaine concernant notre projet Mercuris' Chest vous dévoile aujourd'hui un peu plus la forêt du nord :

![torin_temple](torin_temple-300x225.png)

Complètement différente de celle dont je vous parlais il y a trois semaines, cette forêt est peuplée de créatures fantômatiques qu'il faudra délivrer d'une malédiction.

Vous l'aurez compris, le projet avance avance beaucoup ces temps-ci !
