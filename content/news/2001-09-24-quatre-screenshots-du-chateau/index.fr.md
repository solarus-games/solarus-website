---
date: '2001-09-24'
excerpt: "Salut à tous ! Les dérangements causés par la fermeture de Babeloueb n'ont pas empêcher Zelda : Mystery of Solarus de se construire un peu plus...."
tags:
  - solarus
title: Quatre screenshots du Château
---

Salut à tous !

Les dérangements causés par la fermeture de Babeloueb n'ont pas empêcher Zelda : Mystery of Solarus de se construire un peu plus. Le jeu a même beaucoup avancé depuis le week-end dernier. Le quatrième donjon est totalement terminé depuis pas mal de temps. Tout se qui se passe entre ce donjon et le cinquième est même quasiment prêt ! Et c'est pourtant loin d'être court...

Une surprise se produit à la fin du donjon 4, et c'est en rapport avec le scénario (souvenez-vous : quatre enfants ont été enlevés). Il s'agit en quelque sorte... d'un rebondissement dans le scénario. Cette surprise est d'ailleurs de taille ! Elle ne sera pas révélée avant la sortie de la version finale du jeu, donc ne comptez pas sur moi pour vous en parler ! Ben oui, il faut bien garder un peu de surprises pour le jeu final.

Voici maintenant quatre nouveaux screenshots de mes récentes avancées :

[](solarus-ecran40.png)

[](solarus-ecran41.png)

[](solarus-ecran42.png)

[](solarus-ecran43.png)

Les connaisseurs auront sans difficulté reconnu le Château d'Hyrule !
