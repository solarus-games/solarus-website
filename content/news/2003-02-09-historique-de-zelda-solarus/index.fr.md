---
date: '2003-02-09'
excerpt: 'Vous vous demandez peut-être comment s''est passé le développement de Zelda : Mystery of Solarus ? Comment tout a commencé ? Qui a fait quoi ?...'
tags:
- solarus
title: Historique de Zelda Solarus
---

Vous vous demandez peut-être comment s'est passé le développement de Zelda : Mystery of Solarus ? Comment tout a commencé ? Qui a fait quoi ? Comment est né le site ? Si c'est le cas, votre curiosité va être satisfaite. Nous vous avons en effet dressé un historique des principales étapes de la création du jeu et du développement du site.

[Historique de Zelda Solarus](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=historique)
