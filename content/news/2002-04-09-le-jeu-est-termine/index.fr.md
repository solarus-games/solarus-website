---
date: '2002-04-09'
excerpt: 'Nous sommes fiers de vous annoncer Zelda : Mystery of Solarus est terminé depuis cet après-midi ! Les testeurs sont actuellement en train de faire...'
tags:
- solarus
title: Le jeu est terminé !
---

Nous sommes fiers de vous annoncer Zelda : Mystery of Solarus est terminé depuis cet après-midi !

Les testeurs sont actuellement en train de faire leur boulot et ont 16 jours pour finir le jeu en trouvant le maximum de bugs.

N'oubliez pas le grand rendez-vous du 26 avril, avec la sortie de la version finale !
