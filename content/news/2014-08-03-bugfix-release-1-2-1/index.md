---
date: '2014-08-03'
excerpt: ' A bugfix release named Solarus 1.2.1 is available! It addresses a few issues with recent features that were not working correctly, like custom...'
tags:
- solarus
title: Bugfix release 1.2.1
---

A bugfix release named Solarus 1.2.1 is available!

It addresses a few issues with recent features that were not working correctly, like custom entities, and a problem of corrupted image when creating a new quest with the editor.

- [Download Solarus 1.2](http://www.solarus-games.org/download/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor/ "Solarus Quest Editor")
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html "LUA API documentation")

## Changes in Solarus 1.2.1

- Fix entity:is_in_same_region() giving wrong results (#500).
- Fix custom_entity:set_can_traverse() giving opposite results.
- Fix custom_entity:on_interaction() not always called.
- Fix custom_entity sprite collision issues with some entities (#536).
- Fix a crash in enemy:restart() when the enemy is dying (#558).
- Fix hero:set_tunic_sprite_id() resetting the direction to right (#511).
- Fix timer:get_remaining_time() always returning 0 (#503).
- Fix declaring global variables from a map script (#507).
- Fix the hero sometimes moving while no keys are pressed (#513). By xethm55.
- Fix on_joypad events not always working (#519). By xethm55.
- Add an error when a hero sprite animation is missing (#485). By Nate-Devv.

## Changes in Solarus Quest Editor 1.2.1

- Fix corrupted image in quest created by Quest > New quest (#548).
- Fix tiles created on invisible layer (#508). By Maxs1789.
- Fix crash when an NPC sprite does not have 4 directions (#510). By Maxs1789.
