---
date: '2002-05-19'
excerpt: FAQ = Frequently Asked Questions, c'est-à-dire une page où vous trouverez les réponses aux questions que je reçois tous les jours par e-mail....
tags:
- solarus
title: Mise en ligne de la FAQ
---

FAQ = Frequently Asked Questions, c'est-à-dire une page où vous trouverez les réponses aux questions que je reçois tous les jours par e-mail. Pensez donc à lire la FAQ avant de me contacter, car il y a de fortes chances pour que votre question y soit déjà.

[Lire la FAQ](http://www.zelda-solarus.com/interact.php3?page=faq)
