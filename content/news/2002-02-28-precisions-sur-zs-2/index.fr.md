---
date: '2002-02-28'
excerpt: C'est décidé environ à 40% de créer un deuxième Zelda après celui qui est en cours de création. Le développement de ZS2 devrait prendre...
tags:
- solarus
title: Précisions sur ZS-2
---

C'est décidé environ à 40% de créer un deuxième Zelda après celui qui est en cours de création. Le développement de ZS2 devrait prendre beaucoup moins de temps puisqu'il serait créé avec The Games Factory. Les possibilités de ce soft nous permettraient de recréer la puissance du moteur de Ocarina of time, je parle bien sûr du fonctionnement du jeu, pas de la 3D...

L'actu ZS bouge beaucoup en ce moment, on est un peu en panique à cause de la date de sortie. On va tout faire pour la respecter mais comme nous ne sommes pas des machines, ne soyez pas fâché contre nous si il y'a un retard. Ce qui m'étonnerait quand même puisque nous prévoyons de le terminer théoriquement le 26 Mars... Le mois suivant, les tests approfondis commencent et le débogage également.

Comme vous le voyez, un agenda bien rempli et des tonnes de travail encore à faire. Allez, encore 57 jours à attendre !
