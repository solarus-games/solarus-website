---
date: '2011-12-10'
excerpt: The completed version of Zelda Mystery of Solarus DX will be available soon. Here is recent trailer showing a lot of game...
tags:
  - solarus
title: Zelda Mystery of Solarus DX is almost finished
---

The completed version of _Zelda Mystery of Solarus DX_ will be available soon. Here is recent trailer showing a lot of game sequences!

{{< video "https://www.youtube.com/watch?v=BUxREyXILLs" >}}

The French version will be released on December 16th. There is no release date (yet) for the English version, but I hope it will be ready in few weeks. Any help is welcome for other translations. Feel free to contact me. If you already contacted me for a translation between the demo release and now, it's possible that I did not answer you, Sorry about that. That's because I didn't know what I wanted (translate the old demo? make a new demo? wait for the completed game?). Finally, I chose the third option. So please accept my apologies and try again, the game is ready for translations now :)

Because releasing ZSDX was the priority in 2011, I've been working a lot on this quest in the last months, and there was no important changes in the engine or the quest editor since the scripted enemies. New big changes in the engine and the quest editor were not necessary for our team to finish games like ZSDX and ZSXD. But after the release, I intend to improve the format of quest files and stabilize the quest editor so that you can also make your own quests with Solarus. I have plans about that, and I will update this development blog to keep you informed.
