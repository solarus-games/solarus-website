---
date: '2014-08-21'
excerpt: 'UPDATE: a bugfix version 1.3.1 was just released. It fixes an error when opening a newly created sprite, and improves the sorting of resources with...'
tags:
  - solarus
title: Solarus 1.3 is out, now with a sprite editor!
---

UPDATE: a bugfix version 1.3.1 was just released. It fixes an error when opening a newly created sprite, and improves the sorting of resources with numbers in their id in the quest tree.

A new release of Solarus is available: version 1.3!

The main improvement of Solarus 1.3 is the graphical sprite editor integrated in Solarus Quest Editor. You will no longer have to edit sprite sheet data files by hand!

[Sprite Editor](sprite_editor-1024x576.png "Screenshot of Solarus Quest Editor 1.3, modifying the sprite of an enemy in ZSDX.)

The sprite editor was primarily developed by Maxs. You can create and edit sprite sheets. A tree shows the hierarchy of animations and direction of your sprite. And below the tree, you can edit the animation and direction that are selected in the tree.

The main changes of this release are in the quest editor. Another important improvement is that tile patterns can now be identified by a string instead of auto-generated integers. This allows to more easily maintain different similar tilesets.

There are also a few new features in the engine and in the Lua API, like more customizable switches, but nothing that breaks compatibility of your scripts.

- [Download Solarus 1.3](http://www.solarus-games.org/download/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor/ 'Solarus Quest Editor')
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html 'LUA API documentation')
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

As always, our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games. In ZSDX, there is also a new world minimap made by Neovyse.

- [Download Zelda Mystery of Solarus DX 1.9](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.9](http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/ 'Download ZSXD')

## Here is the full changelog. Changes in Solarus 1.3

Lua API changes that do not introduce incompatibilities:

- Add mouse functions and events (experimental, more will come in Solarus 1.4).
- Add a method sprite:get_animation_set_id() (#552).
- Add a method sprite:has_animation() (#525).
- Add a method sprite:get_num_directions().
- Add a method hero:get_solid_ground_position() (#572).
- Add a method switch:get_sprite().
- Allow to **customize the sprite and sound of switches** (#553).
- Add a method enemy:get_treasure() (#501).

Bug fixes:

- Fix the write directory not having priority over the data dir since 1.1.
- Fix pickable/destructible:get_treasure() returning wrong types.
- Fix custom entity collision detection when the other is not moving (#551).
- Allow to call map methods even when the map is not running.

## Changes in Solarus Quest Editor 1.3

New features:

- Add a **sprite editor** (#135). By Maxs.
- Add a zoom level of 400%. By Maxs.
- Add keyboard/mouse zoom features to sprites and tilesets. By Maxs.
- Add **Lua syntax coloring** (#470). By Maxs.
- Add a close button on tabs (#439). By Maxs.
- Rework the **quest tree** to show the file hierarchy and Lua scripts. By Maxs.
- Add specific icons for each resource type in the quest tree.
- Move the entity checkboxes to the map view settings panel. By Maxs.
- Allow to **change the id of a tile pattern** in the tileset editor (#559).
- Don't initially maximize the editor window.

Bug fixes:

- Fix converting quests to several versions in one go.

## Incompatibilities

These improvements involve changes that introduce slight incompatibilities in the format of data files, but no incompatibility in the Lua API. Make a backup, and then see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide) on the wiki to know how to upgrade.

## Changes in Zelda Mystery of Solarus DX 1.9

New features:

- New world minimap. By Neovyse.
- Make cmake paths more modifiable. By hasufell.

Bug fixes:

- Fix direction of vertical movement on joypad in menus (#85). By xethm55.
- Clarify license of some files.

## Changes in Zelda Mystery of Solarus XD 1.9

New features:

- Make cmake paths more modifiable. By hasufell.

Bug fixes:

- Fix direction of vertical movement on joypad in menus.
- Clarify the license of some files.
