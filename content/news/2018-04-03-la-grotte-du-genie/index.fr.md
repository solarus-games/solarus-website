---
date: '2018-04-03'
excerpt: "Cette semaine, il est temps de vous montrer le donjon 2 de notre remake de Link's Awakening : la grotte du génie. Je pense que vous n'aurez pas de..."
tags:
  - solarus
title: La grotte du génie
---

Cette semaine, il est temps de vous montrer le donjon 2 de notre remake de Link's Awakening : la grotte du génie. Je pense que vous n'aurez pas de mal à reconnaitre ces deux images si vous avez déjà joué au jeu original.

![](1-300x240.png)

![](2-300x240.png)

N'hésitez pas à donner votre avis !
