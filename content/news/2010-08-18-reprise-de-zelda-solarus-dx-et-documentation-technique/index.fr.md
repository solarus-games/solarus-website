---
date: '2010-08-18'
excerpt: Après plusieurs mois de développement au ralenti pour des raisons personnelles, familiales et professionnelles (oui, tout ça cumulé :P), Zelda...
tags:
  - solarus
title: Reprise de Zelda Solarus DX et documentation technique
---

Après plusieurs mois de développement au ralenti pour des raisons personnelles, familiales et professionnelles (oui, tout ça cumulé :P), Zelda Mystery of Solarus DX a pu reprendre son rythme de développement normal.

Le fonctionnement interne du moteur a été beaucoup amélioré. De nombreuses corrections ont été apportées, souvent grâce au travail des testeurs de l'équipe. Certaines parties du moteur ont été entièrement refaites, en particulier toute la gestion des actions de Link, afin de faciliter la suite du développement et de créer moins de bugs lorsque le moteur évolue.

D'autre part, j'ai réalisé beaucoup de documentation technique (en anglais) qui explique notamment comment sont organisées les [données du jeu](http://www.solarus-engine.org/2010/08/06/quest-files-specification/) (les maps, les dialogues, les sprites etc?), comment vous pouvez [vous procurer](http://www.solarus-engine.org/source-code) la version de développement du [code source](http://svn.solarus-engine.org) (qui est libre, rappelons-le), mais aussi comment utiliser [l'éditeur de maps](http://www.solarus-engine.org/solarus/quest-editor) qui est pour l'instant disponible uniquement en version de développement. Attention, tous ces sujets sont techniques et surtout destinés aux développeurs et aux développeurs en herbe, c'est d'ailleurs pour cette raison qu'ils sont en anglais. N'hésitez pas à visiter le [blog de développement](http://www.solarus-engine.org) si ces questions techniques vous intéressent.
