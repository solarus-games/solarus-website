---
date: '2002-02-26'
excerpt: 'Voici les screenshots réclamés par Netgamer sur le forum ! Ils correspondent à la news "Les nouveautés récentes" du 21 février : nouveaux...'
tags:
  - solarus
title: Les screenshots promis !
---

Voici les screenshots réclamés par Netgamer sur le [forum](http://www.zelda-solarus.com/forum.php3) ! Ils correspondent à la news "Les nouveautés récentes" du 21 février : nouveaux chipsets des maisons et du donjon 2, et affichage des Coeurs et des Rubis à l'écran. Dans la colonne de gauche : ce qu'il y avait avant. Dans la colonne de droite : ce qu'il y a actuellement.

[](solarus-ecran58.png)

[](solarus-ecran61.png)

[](solarus-ecran59.png)

[](solarus-ecran57.png)

[](solarus-ecran15.png)

[](solarus-ecran60.png)
