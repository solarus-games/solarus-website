---
date: '2019-04-01'
excerpt: Le nouveau site est finalement prêt, avec plein d'améliorations !
tags:
- website
- solarus
thumbnail: cover.jpg
title: Zelda Solarus devient Solarus-Games !
---

Comme annoncé dans le [précédent article](/fr/news/2019-03-01-moins-de-zelda-plus-de-solarus), Zelda Solarus change de nom !

La version précédente de Zelda Solarus, réalisée avec Wordpress, commençait à montrer son âge. Wordpress était peu pratique, et nous voulions faire en sorte que les passionnés puissent s'impliquer dans le projet.

Ce projet de nouveau site était espéré depuis très longtemps, mais nous n'avions pas encore eu le temps ni les moyens de nous y consacrer. Les premières maquettes avaient été faites dès 2013. Intervint alors Binbin, membre de longue date de la communauté Solarus, qui a déjà participé au développement de nos jeux et de la version précédente du site. Il a écrit un nouveau moteur de site Internet, basé sur le framework Koseven, et qui utilise des fichiers markdown. Nous voulions créer notre propre moteur de site web pour répondre parfaitement à nos besoins et avoir une liberté totale.

![Logo de Kokori](kokori_logo.png)

Le moteur du nouveau site est baptisé **Kokori**, un jeu de mots entre *Kokiri*, le framework *Koseven* sur lequel il est basé, et *coq-aur-riz*, plat que Binbin apprécie beaucoup. Kokori est libre et open-source, de même que le contenu du site.

- [Source code du moteur du site Solarus](https://gitlab.com/solarus-games/kokori)
- [Source code du contenu du site Solarus](https://gitlab.com/solarus-games/solarus-website-pages)

## Nouveautés

Vous pouvez dès à présent découvrir les nombreuses améliorations du nouveau site Solarus :

- **Une présentation moderne et attrayante** de Solarus et Solarus Quest Editor, un préalable indispensable, que vous soyez joueur ou créateur !
- **Une bibliothèque de quêtes :** la liste des jeux réalisés avec Solarus, avec la possibilité de rechercher et de filtrer selon des critères comme la langue. Ces jeux peuvent être es Zelda ou non, et incluent ceux de l'équipe mais aussi les projets les plus marquants de la communauté. Chaque quête dispose de sa propre page dédiée, moderne et professionnelle, qui donne tous les détails sur le jeu et permet bien entendu de le télécharger.
- **Une bibliothèque de packs de ressources :** De la même façon que les jeux, vous pouvez télécharger des packs de graphismes, de musiques et de scripts qui vous serviront de base pour créer vos projets de jeux. À l'heure actuelle : le pack de ressources Zelda: A Link to the Past ou encore le pack de ressources libres officiel Solarus.
- **Tutoriels :** Tout pour vous apprendre à créer une quête avec Solarus si vous avez envie de vous lancer !
- **Un site multilingue :** Solarus-Games est disponible en français et en anglais. La version anglaise correspond à l'ancien blog de développement Solarus (qui n'existait qu'en anglais jusqu'à présent), tandis que la version française est le nouveau Zelda Solarus.

Ce nouveau site Solarus est encore très récent, donc informez-nous si vous rencontrez un bug.

## Nous avons besoin de votre aide

Désormais, n'importe qui peut contribuer à l'amélioration du site, bien que Binbin se réserve la décision finale d'intégrer ou non les modifications. Nous avons besoin de contributeurs pour ces parties :

- **Kokori :** Si vous pensez que vous pouvez améliorer le moteur du site Solarus, Binbin vous accueilera avec joie.
- **Traductions :** Tout le monde peut traduire le moteur et le contenu du site.
- **Tutoriels :** Nous avons besoin d'aide pour écrire des tutoriels destinés aux apprentis créateurs de quêtes Solarus.

Comment contribuer ? Vous trouverez toutes les informations nécessaires sur la [page de contribution](/fr/development/how-to-contribute). N'hésitez pas à nous rejoindre sur [Discord](https://discord.gg/PtwrEgZ) pour discuter avec nous et offrir votre aide.

![Icone de tutoriel](tutorial_icon.png)

## L'ancien contenu sur les Zelda officiels

Avec cette nouvelle version, Zelda Solarus change de nom pour devenir Solarus-Games, se focalise sur la création de jeux et ne traite plus des Zelda officiels.

Que deviennent alors les anciens contenus sur les Zelda officiels? Ils restent valables et précieux, même s'ils manquent de mises à jour depuis trop longtemps. On pense notamment à toutes nos solutions complètes, détaillées et de qualité. Nous avons donc choisi de les offrir avec plaisir aux copains de [Zeldaforce](https://zeldaforce.net/), en remerciement pour tout le travail réalisé par Binbin dans le développement de ce nouveau site Solarus-Games.

Aucun changement en revanche concernant le forum. Le [forum Solarus-Games](http://forum.solarus-games.org) reste le lien de discussion préférentiellement en anglais, tandis que le [forum Zelda Solarus](http://forums.zelda-solarus.com), continue d'être le lieu de discussions en français.

## En conclusion

Mes amis, soyez les bienvenus sur Solarus-Games. Le nom du site a peut-être changé, mais pas notre passion ni notre envie de la partager. Nous sommes fiers de vous présenter aujourd'hui cette refonte moderne et dédiée aux jeux amateurs.

Commencez par aller découvrir la [bibliothèque de quêtes](/fr/games) : il y a déjà quelques projets de la communauté qui sont très prometteurs et dont vous ignoriez sans doute l'existence jusqu'ici :)
nous sommes impatients de voir la communauté continuer de s'agrandir et les projets amateurs se multiplier.
