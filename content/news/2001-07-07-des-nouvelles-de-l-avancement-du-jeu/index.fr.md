---
date: '2001-07-07'
excerpt: Le jeu avance ! En ce moment je suis en train de programmer le troisième donjon. A ce stade de développement, de nombreux objets repris de Zelda 3...
tags:
- solarus
title: Des nouvelles de l'avancement du jeu
---

Le jeu avance ! En ce moment je suis en train de programmer le troisième donjon. A ce stade de développement, de nombreux objets repris de Zelda 3 font déjà leur apparition dans le jeu : Arc, Lanterne et bien d'autres, que vous pouvez pour certains voir sur le screenshot de l'écran de Pause (voir la [galerie](http://www.zelda-solarus.com/galerie.php3)) !

Thomas avance lui aussi, il est en train de préparer des nouveaux chipsets tirés de Zelda 3 pour la suite du jeu...

Pour l'instant je ne peux vous donner aucune date de sortie ni du jeu final, ni d'une éventuelle nouvelle démo, puisque le jeu sera très long et il y a encore beaucoup à faire (10 donjons si tout va bien). Tout ce que je peux vous dire c'est que je profite de ces vacances pour avancer énormément dans la programmation. Le donjon 3 devrait être fini d'ici la semaine prochaine. Ce qui promet de nouveaux screenshots dès les prochains jours !
