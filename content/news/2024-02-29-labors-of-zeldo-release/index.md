---
date: '2024-02-27'
excerpt: The third fangame by ZeldoRetro, following on from the previous ones, has been released.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: Release for The Labors of Zeldo
---

In _The Labors of Zeldo_, you can explore the unreleased projects that ZeldoRetro made using the Solarus Engine, compiled into a single game.

And yes, it is _Zeldo_ with a O, not a A. Have fun!

## How to play

1. Download [Solarus Launcher](/en/download/#gamer-package).
2. Download [the game](/en/games/the-labors-of-zeldo/) (as a `.solarus` file).
3. Open the `.solarus` file with Solarus Launcher.

You may share your experience on the [game's Discord channel](https://discord.gg/CtEnhPRs) with the community.

{{< game-thumbnail "the-labors-of-zeldo" >}}

## Source code and bug reports

The demo's source code is available [on GitHub](https://github.com/ZeldoRetro/labors_zeldo). You can report bugs in the game's [GitHub issues page](https://github.com/ZeldoRetro/labors_zeldo/issues) by creating issues. Please add any information that might help us: steps to reproduce the bug, screenshots or videos, etc.
