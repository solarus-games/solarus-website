---
date: '2024-02-27'
excerpt: Le troisième fangame de ZeldoRetro, dans la continuité des précédents, est sorti.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: Sortie de The Labors of Zeldo
---

Dans _The Labors of Zeldo_, vous pouvez explorer les projets inédits réalisés par ZeldoRetro à l'aide du moteur Solarus, compilés en un seul jeu.

Et oui, c'est _Zeldo_ avec un O, pas un A. Amusez-vous bien !

## How to play

1. Téléchargez [Solarus Launcher](/fr/download/#gamer-package).
2. Téléchargez [le jeu](/fr/games/the-labors-of-zeldo) (sous forme de fichier `.solarus`).
3. Ouvrez le fichier `.solarus` avec Solarus Launcher.

Venez partager votre expérience sur le jeu avec la communauté dans la [discussion Discord](https://discord.gg/TPWDr3HG).

{{< game-thumbnail "the-labors-of-zeldo" >}}

## Source code and bug reports

Le code source de la démo est disponible [sur GitHub](https://github.com/ZeldoRetro/labors_zeldo). Vous pouvez signaler des bugs dans la [page des problèmes GitHub](https://github.com/ZeldoRetro/labors_zeldo/issues) du jeu en créant des problèmes. Merci d'ajouter toute information qui pourrait nous aider : étapes pour reproduire le bug, captures d'écran ou vidéos, etc.
