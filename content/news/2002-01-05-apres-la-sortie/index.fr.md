---
date: '2002-01-05'
excerpt: Et oui ! Après la sortie du jeu complet ? Qu'est-ce qui se passera ? Et bien, nous continuerons l'activité de ce site où nous consacrerons...
tags:
- solarus
title: Après la sortie ?
---

Et oui ! Après la sortie du jeu complet ? Qu'est-ce qui se passera ? Et bien, nous continuerons l'activité de ce site où nous consacrerons d'autres pages par exemple aux quelques astuces du jeu et bien d'autres rubriques !

Nous annonçons que la rubrique FONDS D'ECRANS va bientôt ouvrir ses portes, nous avons préparé 6 wallpapers différents pour votre bureau !

Et enfin, la rubrique MP3 aussi ne devrait pas tarder avec une dizaine de musiques originales du jeu en qualité CD et peut-être même des inédits !!!
