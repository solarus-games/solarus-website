---
date: '2009-07-16'
excerpt: "En quelques jours, le développement de notre projet Zelda : Mystery of Solarus DX a beaucoup avancé. Avant tout, voici l'entrée du premier donjon..."
tags:
  - solarus
title: Des images du premier donjon
---

En quelques jours, le développement de notre projet Zelda : Mystery of Solarus DX a beaucoup avancé.
Avant tout, voici l'entrée du premier donjon (celui de la future démo), qui a gagné en allure :) :

![](http://www.zelda-solarus.com/images/zsdx/dungeon1_outside.png)

A l'intérieur, toutes les salles du premier donjon sont faites. Pour être plus précis, tous les décors ont été posés, ainsi que les ennemis, les coffres et la plupart des éléments. Il reste donc à les programmer, c'est-à-dire écrire les scripts qui vont définir les énigmes, ce qui se passe lorsqu'on appuie sur un interrupteur, l'apparition des coffres, l'ouverture des portes... Pour l'instant, si vous aviez la possibilité de jouer, vous pourriez traverser toutes les pièces sans rien faire :P.

![](http://www.zelda-solarus.com/images/zsdx/dungeon1_3.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon1_1.png)

Les salles sont bien sûr reproduites d'après Zelda : Mystery of Solarus premier du nom, avec cependant beaucoup d'améliorations architecturales afin de donner un aspect plus esthétique et une meilleure cohérence. C'est notamment le cas du dernier étage de ce donjon qui a été entièrement repensé.

Les labyrinthes et les énigmes sont repris et adaptés. Les labyrinthes que je juge les moins intéressants sont remplacés par des phases de combats, séquences qui prennent tout leur intérêt dans cette version DX où les combats sont beaucoup plus agréables à jouer. Il y aura donc plus de combats, sans diminuer le nombre d'énigmes puisque seuls les labyrinthes rébarbatifs seront enlevés. Au contraire, vous verrez que les énigmes sont plus intéressantes et plus variées, en tout cas je fais le maximum pour :)

Prochaines étapes : programmer les événements du donjon, ce qui nécessitera de compléter deux petites choses que le moteur ne gère pas encore : les ouvertures et fermetures de portes (verrouillées ou non) et les tapis roulants. Puis viendra la création du boss et du mini-boss, qui donneront lieu à des news prometteuses :P.
Bref, une fois que tout ceci sera fait, on commencera à y voir plus clair concernant la date de sortie de la démo, toujours annoncée pour 2009 rappelons-le :).

![](http://www.zelda-solarus.com/images/zsdx/dungeon1_2.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon1_4.png)

[Galerie d'images](http://www.zelda-solarus.com/jeu-zsdx-images)
