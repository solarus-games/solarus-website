---
date: '2015-08-16'
excerpt: Solarus has now an official Twitter account. It is no more Christopho's one, even if both are obviously tightly linked. You can now follow us if you...
tags:
- solarus
title: Official Twitter account
---

Solarus has now an official Twitter account. It is no more Christopho's one, even if both are obviously tightly linked. You can now follow us if you still don't.

![follow_on_twitter](follow_on_twitter-300x75.png)

And remember that we also have an official Facebook account, which by now will just forward the twits from its Twitter counterpart.

![follow_on_facebook](follow_on_facebook-300x75.png)
