---
date: '2018-12-23'
excerpt: Le cadeau de Noël Solarus est arrivé ! Une nouvelle version de notre moteur de jeu Solarus et de l'éditeur de quêtes Solarus Quest Editor...
tags:
- solarus
thumbnail: cover.jpg
title: Solarus 1.6 est disponible
---

Le cadeau de Noël Solarus est arrivé !

Une nouvelle version de notre moteur de jeu Solarus et de l'éditeur de quêtes Solarus Quest Editor vient de sortir. Tout l'équipe y travaille d'arrache-pied depuis deux ans et demi.

![](quest_editor_screenshot-1024x576-300x169.png)

Nous sommes très fiers de vous présenter Solarus 1.6, qui apporte énormément de nouvelle fonctionnalités aux créateurs de jeux, comme par exemple :

- Utilisation d'**OpenGL** pour la partie graphique et support des**shaders GLSL** (merci à Std::gregwar et Vlag)
- Support de l'**éditeur de texte externe** de votre choix. Intégration de **Zerobrane** : autocomplétion, points d'arrêt, inspection de la pile (merci à Std::gregwar)
- Nombreuses améliorations de l'éditeur de map, avec notamment le **générateur de contours** (autotiles), le **remplacement de tiles** et le support des **tilesets multiples**
- Nombreuses améliorations de l'éditeur de tilesets, avec la **sélection multiple**, les motifs animés avec **nombre de frames et délai au choix**
- Nombreuses améliorations de l'éditeur de sprites
- Possibilité d'**importer** des ressources d'autres quêtes
- De magnifiques nouveaux **tilesets libres** (Zoria par DragonDePlatino, Ocean's Heart par Max Mraz)
- **Nouvelles polices** pixel libres (par Wekhter)
- États du héros personnalisés pour un contrôle avancé du héros
- Innombrables nouvelles fonctionnalités dans l'API Lua

Sachez aussi que Solarus 1.6 est entièrement compatible avec les quêtes 1.5. Aucune action particulière n'est nécessaire pour mettre à jour votre projet vers Solarus 1.6.

- [Télécharger Solarus 1.6](http://www.solarus-games.org/engine/download/)
- [Pack de ressources Zelda A Link to the Past](https://gitlab.com/solarus-games/solarus-alttp-pack/-/archive/v1.6.0/solarus-alttp-pack-v1.6.0.zip) pour Solarus
- [Pack de ressources libres](https://gitlab.com/solarus-games/solarus-free-resource-pack/-/archive/v1.6.0/solarus-free-resource-pack-v1.6.0.zip) pour Solarus

De nouveaux tutoriels vidéos vont arriver prochainement pour vous initier à la création de jeux avec Solarus.

Je vous laisse découvrir l'incroyable [Trailer Solarus 1.6](https://www.youtube.com/watch?v=vt07FwzLo9A) réalisé par Olivier Cléro !

Joyeux Noël à toutes et à tous !
