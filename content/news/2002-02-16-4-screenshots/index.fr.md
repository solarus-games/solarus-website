---
date: '2002-02-16'
excerpt: Salut à tous ! L'ancien niveau 8 me plaisait pas du tout, on voyait rien, le chipset comportait plein d'erreur, c'était l'horreur quoi ! C'est...
tags:
  - solarus
title: 4 screenshots !!!
---

Salut à tous !

L'ancien niveau 8 me plaisait pas du tout, on voyait rien, le chipset comportait plein d'erreur, c'était l'horreur quoi ! C'est pourquoi j'ai tout recommencé aujourd'hui et j'ai travaillé d'arrache-pied pour refaire un chipset et le niveau 8.

On ne vous dit rien sur ce donjon malheureusement sinon, on vous dévoilerait trop de détails sur le scénario... Cette partie du jeu est la plus palpitante car on approche de la fin et d'un affrontement tant attendu. Le jeu ne comporte que 9 donjons mais longs et progressivement difficiles de quoi vous occuper...

Trêve de bavardages, voici les écrans en question (cliquez dessus pour agrandir) :

[](zs-n8-01.gif)

[](zs-n8-02.gif)

[](zs-n8-03.gif)

[](zs-n8-04.gif)
