---
date: '2024-01-14'
excerpt: We are unvealing new pictures of our next creation with a widescreen format worthy of the name!
tags:
  - games
  - mercuris
thumbnail: thumbnail.png
title: "New pictures for Zelda : Mercuris' Chest"
---

It's been a long time since we last gave any news about our fangame The Legend of Zelda: Mercuris' Chest.

After we released the demo on April 1st, 2023, we continued to work on the full game; and throughout this year, we will communicate on the numerous new features that we are adding as we go.

## Big screen

First big announcement, as you see in the screenshots of the full game in development, we have decided to offer you a widescreen experience!

![Well guarded village](screen_1.png)

![Talk at Chrono Town](screen_2.png)

![Visiting Gorons](screen_3.png)

Like other games like Ocean's Heart, no more 4/3 display! Mercuris' Chest is a fangame where you will experience an amazing spectacle on the entire display of your screen.

## Release date

As for the project release date, we do not have a precise idea on when it will be yet. Stay tuned for updates as we should be talking about it very soon.

In the meantime, we are actively working to make this game a truly different experience from our previous creations.
