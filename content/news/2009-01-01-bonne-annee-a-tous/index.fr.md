---
date: '2009-01-01'
excerpt: Chers Solarusiens, Nous espérons que vous avez passé de joyeuses fêtes. En ce Jour de l'An, toute l'équipe de Zelda Solarus vous souhaite...
tags:
  - solarus
title: Bonne année à tous !
---

Chers Solarusiens,

Nous espérons que vous avez passé de joyeuses fêtes. En ce Jour de l'An, toute l'équipe de Zelda Solarus vous souhaite **une bonne et heureuse année 2009** :).

2008 a été riche en émotions avec l'annonce de notre nouvelle création Zelda Solarus DX. Mais 2009 devrait être une année palpipante car je peux désormais l'annoncer officiellement : une **démo jouable** de Zelda : Mystery of Solarus DX verra le jour cette année ! :D

En effet, grâce à l'avancement rapide du projet, le développement se concrétise et on peut envisager de sortir une démo. Nous vous donnerons plus de détails sur le contenu de cette démo (et sa date !) un peu plus tard ;).

Passez une bonne année 2009 avec Zelda Solarus ^\_^.
