---
date: '2016-11-29'
excerpt: A new bugfix release of Solarus, Solarus Quest Editor and of the Sample Quest was just published!A few annoying bugs were fixed.Additionally, we...
tags:
  - solarus
title: Bugfix release 1.5.2 and improved sample quest!
---

A new bugfix release of Solarus, Solarus Quest Editor and of the Sample Quest was just published!A few annoying bugs were fixed.

Additionally, we added a lot of content to the official Sample Quest, including enemies, NPCs, musics and sounds (thanks Diarandor!). And there is also now an experimental clickable HUD (thanks Vlag!). The sample quest can now be playable without keyboard.

Finally, Solarus Quest Editor and the Solarus launcher GUI are now available in Spanish (thanks Diarandor)!

- [Download Solarus 1.5](http://www.solarus-games.org/engine/download/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/)
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html 'LUA API documentation')

## Changes in Solarus 1.5.1

- Launcher: add Spanish translation (thanks Diarandor!).
- Launcher: start the selected quest with Return or double-click (#949).
- Launcher: fix registering quest at quest install time (#948).
- Fix crash when a carried bomb explodes (#953).
- Fix crash when a scrolling teletransporter is incorrectly placed (#977).
- Fix crash when an entity has a wrong savegame variable type (#1008).
- Fix memory leak when creating lots of surfaces (#962).
- Fix cleanup of the quest files at exit.
- Fix error in sol.main.load_settings() when the file does not exist.
- Fix ground ignored after hero:unfreeze() or back to solid ground (#827).
- Fix entity:get_name() returning nil after the entity is removed (#954).
- Improve error messages of surface creations and conversions.
- Chests: set an initial value "entities/chest" to the sprite field.

## Changes in Solarus Quest Editor 1.5.1

- Add Spanish translation (thanks Diarandor!).
- Update maps when renaming musics, enemies and custom entities (#222).
- Fix resizing the console when a sprite editor is open (#215).
- Fix quest reopened even if it was closed in previous session (#220).
- Fix renaming `.it` and `.spc` musics.
- Fix typos in the dialogs and strings editors (#231).
- Fix typo in French translation (#240).
- Map editor: fix update teletransporters checkbox in tile edit dialog (#221).
- Map editor: fix setting destructible objects non liftable (#247).
- Map editor: save all only if necessary when renaming a destination (#219).
- Map editor: fix tileset view scrollbar position lost sometimes (#229).
- Map editor: fix escape accepting resize/move instead of cancelling it (#217).
- Tileset editor: fix selected pattern view after refreshing the image (#218).
- Text editor: fix freeze when indenting selected lines sometimes.
- Text editor: don't indent empty lines.
- Initial quest: put the solarus logo script in scripts/menus/ (#216).
- Initial quest: fix wrong hero sprite after game-over.

## Changes in Solarus Sample Quest 1.5.1

The official [sample quest](https://github.com/solarus-games/solarus-sample-quest/) is growing bigger, it now deserves its own git repository.

- Add enemies, NPCs, musics and sounds (thanks Diarandor!).
- Add a clickable HUD (thanks Vlag!).
- Put the solarus logo script in scripts/menus/.
- Fix wrong hero sprite after game-over.

Enjoy!

> Update: Solarus Quest Editor was updated to a new version 1.5.2 to fix an annoying issue of nested folders created when you attempt to create a new quest inside the initial quest itself.
