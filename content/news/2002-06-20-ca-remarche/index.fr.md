---
date: '2002-06-20'
excerpt: Après une longue période d'indisponibilité, le site fonctionne à nouveau ! En effet, nous avons été hackés et le mot de passe de la base de...
tags:
- solarus
title: Ca remarche !
---

Après une longue période d'indisponibilité, le site fonctionne à nouveau ! En effet, nous avons été hackés et le mot de passe de la base de données avait changé. Donc impossible d'y accéder.

Quoi qu'il en soit, nous avons récupéré la base sans perdre de données donc tout va bien. Nous avons également renforcé la sécurité des pages pour éviter d'autres problèmes à l'avenir.
