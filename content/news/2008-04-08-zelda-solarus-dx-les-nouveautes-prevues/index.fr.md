---
date: '2008-04-08'
excerpt: Bonjour à tous, Aujourd'hui, je suis heureux de vous dévoiler le détail des améliorations prévues dans la version DX de Mystery of...
tags:
  - solarus
title: 'Zelda Solarus DX : les nouveautés prévues'
---

Bonjour à tous,

Aujourd'hui, je suis heureux de vous dévoiler le détail des améliorations prévues dans la version DX de Mystery of Solarus.

Les changements se situeront pour la plupart au niveau du gameplay, qui est largement à améliorer. Imaginez l'ambiance de Mystery of Solarus avec la jouabilité de Mercuris' Chest !

Comme le laissait suggérer la page [équipe](http://www.zelda-solarus.com/jeux.php?jeu=zsdx&zone=equipe) ouverte depuis quelques jours, il y aura aussi quelques nouveautés du côté des graphismes et des musiques. Et peut-être des nouvelles phases d'explorations comme un nouveau donjon, même si ce n'est pas encore sûr :)...

- [Nouveautés prévues dans la version DX](http://www.zelda-solarus.com/jeux.php?jeu=zsdx&zone=nouveautes)
