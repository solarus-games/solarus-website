---
date: '2006-04-30'
excerpt: Puisque l'actualité Zeldaesque est plutôt calme ces temps-ci, on s'est dit qu'il était temps de publier de nouveaux artworks. Il m'en reste...
tags:
  - solarus
title: Quelques nouveaux artworks pour Mercuris' Chest !
---

Puisque l'actualité Zeldaesque est plutôt calme ces temps-ci, on s'est dit qu'il était temps de publier de nouveaux artworks. Il m'en reste d'autres en réserve, mais ils ne sont pas finis.

[Accéder aux artworks](http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=artworks)

Voici une jolie madame Gerudo très proche de celle d'Ocarina of Time, et un Goron, dont la posture ne vous rappellerait-elle pas notre cher ami gaulois amateur de sangliers ?
Je publie aussi le dessin du capitaine des Pirates, un peuple plutôt barbare habitant l'Est du pays.

À bientôt de nouveaux artworks !

![](http://www.zelda-solarus.com/images/zf/artworks/pirate_mini.jpg)
