---
date: '2023-08-28'
excerpt: A one-week course, led by Christopho, allowed children to learn how to create a game.
tags:
  - games
  - solarus labs
thumbnail: thumbnail.png
title: Solarus was used to teach game development to children
---

Christopho, the creator of Solarus, was invited by **Numhérilab**, the digital space of the [Héricourt media library](https://mediatheque-payshericourt.c3rb.org/index.php) (Haute-Saône, in France), to show a small group of children passionate about video games how they are created. This internship was funded as part of the [Contrat Territoire Lecture](https://www.culture.gouv.fr/Thematiques/Livre-et-lecture/Les-bibliotheques-publiques/Developpement-de-la-lecture-publique/Les-contrats-territoire-lecture-CTL) (CTL), a public program set up for libraries.

![Photo shot during the internship (1)](numherilab_1.jpg)

The internship lasted one week, at the end of August 2023, and allowed 5 children to learn how to create a video game with Solarus. The children loved it, and learned much faster than expected! At the end of the course, they presented their game to their parents, who were able to see the progress made in just one week. Perhaps vocations are born, because some children would like to work in the creation of video games later.

Christopho also enjoyed leading the course, because he likes teaching. You may have noticed it with his series of tutorials on Solarus! He was assisted by Lionel Martin, digital animator, to carry out the internship.

![Photo shot during the internship (2)](numherilab_2.jpg)

It was a conclusive and very enriching experience for everyone, and we hope that there will be others in the future, in Héricourt or elsewhere.
