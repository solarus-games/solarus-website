---
date: '2017-05-17'
excerpt: Zelda Mercuris' Chestse complète peu à peu ! Aujourd'hui, Newlink vous propose cette image tirée du charmantvillage maritime quenotre héros...
tags:
- solarus
title: Le village des flots
---

Zelda Mercuris' Chest se complète peu à peu ! Aujourd'hui, Newlink vous propose cette image tirée du charmant village maritime que notre héros va avoir l'occasion de visiter.

![zora](zora-300x226.png)

Vous aurez ici l'occasion de croiser un peuple bien connu de la série des Zelda ;)
