---
date: '2012-04-05'
excerpt: We are proud to announce a new version of our main creation Zelda Mystery of Solarus DX. This is version 1.5.0, and the quest is now available in...
tags:
  - solarus
title: The English release!
---

We are proud to announce a new version of our main creation [Zelda Mystery of Solarus DX](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Zelda Mystery of Solarus DX'). This is version 1.5.0, and the quest is now available in English!

![](dungeon_8_prickles.png 'Zelda Mystery of Solarus DX screenshot')
[**Download now**](http://www.zelda-solarus.com/jeu-zsdx-download&lang=en) on Zelda Solarus

Big thanks to the [OpenPandora](http://boards.openpandora.org/index.php?/topic/6462-translation-help-needed-for-zelda-solarus-dx/) community who initiated the translation work, and to Jeff, Rypervenche and AleX_XelA who completed it. The translation was a lot of work, with a text file of 6500 lines of dialogs to translate!

We hope that you will enjoy the game. This is the first English-speaking release of the game. Don't hesitate to tell us if you find a typo or a strange dialog: I will publish updates. Translators did a great work, but nothing is perfect! The best way to report errors in dialogs or make some remarks or suggestions is to file an issue on our [github](https://github.com/christopho/solarus/issues).

Zelda Mystery of Solarus DX 1.5.0 changes:

- New language available: English
- Fix two minor issues in dungeon 5

Solarus 0.9.2 changes:

- Fix two bugs with teletransporters

Full changelogs (including previous versionss) of both projects are in the git repository. As of now, future updates will be mentioned on this page.

The website also has a [downloads page](http://www.solarus-games.org/games/downloads/ 'Downloads') now. I have also improved the top navigation bar as well as the right menus. The right menus now provide direct access to our games and to useful links for developers. It's way better than before, but I'm not a an expert and your [feedback](http://www.solarus-games.org/contact/ 'Contact') is welcome ;)
