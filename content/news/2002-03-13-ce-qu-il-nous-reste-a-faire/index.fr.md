---
date: '2002-03-13'
excerpt: A 42 jours de la date de sortie du jeu, il nous reste encore pas mal de boulot. Pour ma part, après avoir été obligé de formater mon disque dur...
tags:
  - solarus
title: Ce qu'il nous reste à faire
---

A 42 jours de la date de sortie du jeu, il nous reste encore pas mal de boulot. Pour ma part, après avoir été obligé de formater mon disque dur et de tout réinstaller, je suis actuellement en train de reprogrammer tous les ennemis du jeu avec le moteur de combat amélioré (gestion aléatoire des objets donnés, diversification des graphismes et des sons des monstres, et correction de quelques bugs). Netgamer bosse quant à lui sur plusieurs trucs à la fois, et il devrait bientôt m'envoyer certains fichiers que j'attends avec impatience (voir ci-dessous) !

Voici un bilan complet des principales tâches qu'il nous reste à terminer pour le jour J, c'est-à-dire le vendredi 26 avril pour ceux qui n'auraient pas encore retenu la date. Les indications "100%" signifient que Netgamer a déjà créé les fichiers mais que je n'ai pas encore pu les programmer car je ne les ai tout simplement pas encore reçus (soit parce que c'est récent, soit à cause de mésaventures avec Windows et de MSN...). Bref, voici la fameuse liste :

- Création de sprites de Link avec ses derniers objets d'équipement (100%)
- Création des cartes et boussoles des donjons (100%)
- Création des nouveaux titres des donjons (100%)
- Création d'un nouvel écran copyright (100%)
- Création du niveau 9 (75%)
- Création du Boss de fin et des dernières maps (0%)
- Création de monstres pour l'extérieur (60%)
- Création de sprites des personnages d'Hyrule (? %)
- Création du programme de lancement du jeu (80%)
- Création des divers fichiers joints avec le jeu (5%)
- Retouches de quelques musiques MIDI (45%)
- Création des O.S.T. en MP3 (15%)

Comme vous pouvez le constater, il nous reste encore beaucoup à faire avant le 26 avril. Pour l'instant on ne peut pas encore parler de retarder le jeu car en travaillant dur, on peut encore y arriver malgré le retard accumulé depuis le mois de février.
