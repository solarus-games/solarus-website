---
date: '2024-03-30'
excerpt: Voici une rétrospective de ce qui s'est passé pour Solarus en 2023.
tags:
  - solarus
  - retrospective
thumbnail: cover.jpg
title: 'Solarus en 2023 : rétrospective'
---

## Introduction

Comme chaque année, nous organisons l'Assemblée Générale de l’association à but non lucratif Solarus Labs. Nous en profitons pour revenir sur l'année écoulée et discuter des projets à venir.

## Projets sur lesquels nous avons travaillé en 2023

### Solarus 2

- Les nouvelles fonctionnalités multijoueurs de Solarus sont prêtes.
- De nouvelles fonctionnalités audio ont été développées.
- La stabilisation est en bonne voie, avec de nombreux tickets clôturés. Deux jeux majeurs utilisent la branche dev (par Max et Metallizer), testent le terrain et signalent les bugs.
- Le nouveau lanceur est toujours en développement.

### _Mercuris' Chest_ renaît de ses cendres

Vingt ans après son annonce, Metallizer a travaillé dessus en secret et a publié une démo au printemps 2023, en utilisant Solarus 1.6.5. Il utilise désormais Solarus 2.0.0 et est désormais au format écran large.

![Capture d'écran de Mercuris' Chest](mercuris-chest-2023.png)

### Stage d'été de développement de jeux

Christopho a été invité par la médiathèque d'Héricourt (France) pour apprendre aux enfants (11 à 13 ans) à réaliser un jeu vidéo avec Solarus, pendant une semaine. Tous les revenus ont été versés sur le compte de l’association.

![Satage d'été](summer-camp-2023.jpg)

### Un thème sombre pour l'éditeur de quêtes

Un thème Qt nommé Qlementine est en développement, et bien que pas encore en v1.0.0, déjà utilisé en production par Filewave. C'est la société pour laquelle Christopho travaille, et ils ont embauché Olivier en tant qu'indépendant pendant 3 mois pour l'aider à terminer le développement de ce thème Qt.

Voici une capture d'écran de Qlementine sur l'application Filewave. Le thème Qt peut être personnalisé : attendez-vous à ce que Solarus Quest Editor se pare des couleurs de Solarus au lieu du bleu standard.

![Camp d'été](filewave-qlementine-2023.png)

### Solarus pour Android

Le développement de l'application Android a progressé. Olivier a réalisé des [maquettes Figma](https://www.figma.com/file/4g0JtgKDJwX1k8awZMqUGL/Solarus-Apps?type=design) et Steve Spear s'est attaqué à l'énorme tâche consistant à procéder à une refonte complète du code de l'interface utilisateur, en passant à Kotlin.

![Solarus pour Android](solarus-android-app-2023.png)

### API statique du site Web

Le site dispose désormais d'une [API statique](https://gitlab.com/solarus-games/solarus-website/-/blob/master/CONTRIBUTING.md#json-static-api) qui permet d'obtenir la liste des jeux Solarus, leurs métadonnées et leur dernière version. Il sera utilisé par les lanceurs desktop et Android pour télécharger et mettre à jour les jeux.

Par exemple, l'URL suivante :

```txt
https://www.solarus-games.org/games/the-legend-of-zelda-mystery-of-solarus-dx/index.json
```

Donnera ce JSON :

```json
{
  "version": "1.0.0",
  "données": {
    "download": "https://gitlab.com/.../zsdx-v1.12.3.solarus",
    "title": "La Légende de Zelda : Le Mystère de Solarus DX",
    "version": "1.12.3"
    // et bien d'autres informations
  }
}
```

L'API statique est construite au moment de la compilation du site, et utilise les API Gitlab et Github pour obtenir les liens de téléchargement du jeu.

### Divers

- _Ocean's Heart_ a dépassé les 100000 ventes sur Steam !
- La communauté a continué à contribuer aux projets liés à Solarus (packs de ressources, jeux, améliorations du moteur).

## En pause

Ces projets étaient en pause en 2023 :

- Portage WASM de Solarus.
- Développement de _The Legend of Zelda : A Link to the Dream_.
- Migration de la documentation Doxygen vers le nouveau site MkDocs.

## Projets prévus pour 2024

**Développement du moteur et des outils:**

- Sortie de Solarus 2.0.
- Sortie de la nouvelle version du lanceur.
- Mise à jour de la documentation avec MkDocs.
- Développement d'applications Android.
- Collaboration avec Recalbox pour supporter le Raspberry Pi.

**Développement de jeu:**

- _Children of Solarus_
- _The Legend of Zelda: Mercuris’ Chest_

**Divers:**

- Nouveau(x) stage(s) d'été Solarus.

**Peut-être, si nous avons le temps :**

- Migration vers un monorepo.
- Portage WASM.

## Conclusion

Nous pensions au départ que 2023 était une année peu remplie, mais en réalité, ce fut une année productive ! Solarus 2.0 est plus proche que jamais !

Merci à tous les membres de la communauté qui rendent ce projet possible.
