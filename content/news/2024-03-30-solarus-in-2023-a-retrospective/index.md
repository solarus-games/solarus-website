---
date: '2024-03-30'
excerpt: Here is a recap of what happened for Solarus in 2023.
tags:
  - solarus
  - retrospective
thumbnail: cover.jpg
title: 'Solarus in 2023: retrospective'
---

## Introduction

As for every year, we hold a general assembly for the Solarus Labs non-profit organization. We take advantage of it to look back on past year and establish upcoming projects.

## Projects we worked on in 2023

### Solarus 2

- The new multiplayer features of Solarus are ready.
- New audio features have been developped.
- Stabilization is on the good tracks, with lots of tickets closed. Two major games use the dev branch (by Max and Metallizer), test the ground and report bugs.
- The new launcher is still in development.

### _Mercuris' Chest_ rises from its ashes

Twenty years later its announcement, Metallizer worked on it secretly, and released a demo in 2023 spring, using Solarus 1.6.5. It uses Solarus 2.0.0 now and is now widescreen.

![Mercuris' Chest screenshot](mercuris-chest-2023.png)

### Game developement summer camp

Christopho has been invited by the Héricourt media library (France) to teach children (11 to 13 years old) how to make a video game using Solarus, during one week . All the revenue went into the association’s bank account.

![Summer Camp](summer-camp-2023.jpg)

### A dark theme for the Quest Editor

A Qt theme named Qlementine is in developement, and albeit not yet in v1.0.0, already used in production by Filewave. This is the company Christopho works for, and they hired Olivier as a freelance for 3 months to help completing the development of this Qt theme.

Here is a screenshot of Qlementine on Filewave's app. The Qt theme can be customized so you can expect Solarus Quest Editor to feature Solarus' colors instead of the standard blue.

![Summer Camp](filewave-qlementine-2023.png)

### Solarus for Android

The development of the Android app has progressed. Olivier made [Figma mockups](https://www.figma.com/file/4g0JtgKDJwX1k8awZMqUGL/Solarus-Apps?type=design) and Steve Spear tackled the huge task of making a whole UI code overhaul, switching to Kotlin.

![Solarus for Android](solarus-android-app-2023.png)

### Website static API

The website now has a [static API](https://gitlab.com/solarus-games/solarus-website/-/blob/master/CONTRIBUTING.md#json-static-api) that allows to get the list of Solarus games, their metadata and their latest version. It'll be used by the desktop and Android launchers to download and update games.

For exemple, the following URL:

```txt
https://www.solarus-games.org/games/the-legend-of-zelda-mystery-of-solarus-dx/index.json
```

Will give this JSON:

```json
{
  "version": "1.0.0",
  "data": {
    "download": "https://gitlab.com/.../zsdx-v1.12.3.solarus",
    "title": "The Legend of Zelda: Mystery of Solarus DX",
    "version": "1.12.3"
    // and many more information
  }
}
```

The static API is built at the website's compile time, and uses Gitlab and Github APIs to get the game download links.

### Miscellaneous

- _Ocean’s Heart_ surpassed 100,000 sales on Steam!
- The community continued to contribute to Solarus-related projects (resource packs, games, engine improvements).

## On pause

These projects were on pause in 2023:

- Solarus WASM port.
- Development of _The Legend of Zelda: A Link to the Dream_.
- Migration of Doxygen documentation to the new MkDocs site.

## Projects Planned for 2024

**Engine and tools development:**

- Solarus 2.0 release.
- New launcher release.
- Documentation update with MkDocs.
- Android app development.
- Collaboration with Recalbox to support the Raspberry Pi.

**Game development:**

- _Children of Solarus_
- _The Legend of Zelda: Mercuris’ Chest_

**Miscellaneous:**

- New Solarus summer camp(s).

**Maybe, if we have time:**

- Migration to a monorepo.
- WASM port.

## Conclusion

We initially thought that 2023 has been a blank year, but actually it was a productive year! Solarus 2.0 is closer than ever before!

Thank you to all community members who make this project possible.
