---
date: '2001-09-15'
excerpt: Honte à moi ! Une des nouvelles images que j'ai mises en ligne récemment (du donjon 4) n'apparaissait pas dans la galerie ! Mille excuses ! Mais...
tags:
  - solarus
title: Oups ; )
---

Honte à moi ! Une des nouvelles images que j'ai mises en ligne récemment (du donjon 4) n'apparaissait pas dans la galerie ! Mille excuses ! Mais maintenant c'est corrigé et vous pouvez contempler ce screenshot au même titre que les autres...

[Visiter la galerie](http://www.zelda-solarus.com/galerie.php3)

À bientôt.
