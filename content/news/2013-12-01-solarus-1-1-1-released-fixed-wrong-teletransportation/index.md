---
date: '2013-12-01'
excerpt: An update of Solarus, Zelda Mystery of Solarus DX and XD was just released! The main problem was a bug in Billy's cave in ZSDX. You could be...
tags:
  - solarus
title: Solarus 1.1.1 released, fixed wrong teletransportation
---

An update of Solarus, Zelda Mystery of Solarus DX and XD was just released!

The main problem was a bug in Billy's cave in ZSDX. You could be teletransported wrongly to a place that you were not supposed to see so early in the game, and more importantly, you could get stuck if you saved at that place. If you have this problem, just download the new version of ZSDX and your savegame will automatically get fixed.

Other annoying issues were fixed, including the heart meter not always updated and the game failing to detect the death of enemies falling into holes.

- [Download Solarus 1.1.1](http://www.solarus-games.org/downloads/download-solarus/ 'Download Solarus')
- [Download Zelda Mystery of Solarus DX 1.7.1](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.7.1](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/ 'Download ZSXD')

## Changes in Solarus 1.1.1

- Fix teletransporters activated while coming back from falling (#346).
- Fix a libmodplug compilation problem due to wrong sndfile.h (#324).
- Fix enemy death detection when falling into hole, lava or water (#350).
- Fix a crash when changing the hero state in block:on_moved (#340).

## Changes in Zelda Mystery of Solarus DX 1.7.1

- Fix the heart meter not updated until the first save/reload (#67).
- Fix wrong teletransportation in Billy's cave after falling in a hole (#66).
- Fix the debugging console no longer working after using F1, F2 or F3.

## Changes in Zelda Mystery of Solarus XD 1.7.1

- Fix Creepers never exploding after they are hurt (#16).
- Fix a typo in English dialogs.
- Fix the debugging console no longer working after using F1, F2 or F3

As always, you can install both games to replace any previous version and your savegames will continue to work.

Thank you for your feedback!
