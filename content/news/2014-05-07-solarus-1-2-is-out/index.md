---
date: '2014-05-07'
excerpt: After six months of development, we are proud to announce the release of the Solarus 1.2!Solarus now uses SDL2. Graphics are GPU-accelerated if...
tags:
  - solarus
title: Solarus 1.2 is out!
---

After six months of development, we are proud to announce the release of the Solarus 1.2!Solarus now uses [SDL2](http://www.libsdl.org/). Graphics are GPU-accelerated if available. Switching to fullscreen is faster, does not change the resolution and keeps the correct pixel ratio. And the window is resizable. There are new pixel scaling filters, including hq4x which is great to play on big screens.

We now support [LuaJIT](http://luajit.org/), the just-in-time Lua compiler. Combined with the accelerated graphics of SDL2, we obtain better performance than ever.

But if you are a quest maker, you will especially be interested by [custom entities](http://www.solarus-games.org/doc/latest/lua_api_custom_entity.html), [streams](http://www.solarus-games.org/doc/latest/lua_api_stream.html) (the new name of conveyor belts), [metatables](http://www.solarus-games.org/doc/latest/lua_api_main.html#lua_api_main_get_metatable) and [repeatable timers](http://www.solarus-games.org/doc/latest/lua_api_timer.html#lua_api_timer_start). I will give more details about these new features in future posts, but you can already see the documentation.

The quest editor was greatly improved. You can now resize multiple entities at once. The map view can be dragged with the middle mouse button and can be zoomed with <kbd>Ctrl</kbd> + wheel. Copy-pasting is more intuitive now, and works much better between different maps.

- [Download Solarus 1.2](http://www.solarus-games.org/download/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor/ 'Solarus Quest Editor')
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html 'LUA API documentation')
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

Our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games. ZSXD is now available in Spanish (beta). Thanks Xexio!

- [Download Zelda Mystery of Solarus DX 1.8](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.8](http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/ 'Download ZSXD')

Here is the full changelog. 153 Github issues were resolved for Solarus 1.2 (this is a new record!) and 20 more for ZSDX and ZSXD 1.8.

## Changes in Solarus 1.2

New features:

- Upgrade to **SDL 2** (#262). Thanks Vlag.
- Accelerate video operations in GPU if available. Expect huge speedups.
- Add the hq2x, hq3 and **hq4x** pixel filter algorithms as new video modes.
- Make the window resizable (#338).
- Use **LuaJIT** if available instead of vanilla Lua for better performance.
- New map entity type: **custom entities, fully scripted** (#323).
- Conveyor belts are now called **streams** and can be non-blocking (#288).
- Collision rules of streams (conveyor belts) are now like holes.
- Rewrite the main loop with a constant timestep (#351).
- Show a dialog box in case of fatal error.
- The "wide" video modes do not exist anymore. SDL2 does the job now.
- Make stairs sounds optional (#364).
- Make more checks in sprite files to avoid crashes (#357).
- Set the default speed of StraightMovement to 32 instead of 0 (#343).
- The size of all map entities must be a multiple of 8 (#358).
- Thrown entities (pots, bombs...) can now fall to a lower layer (#349).
- Running into a crystal or a solid switch now activates it (#193).
- The hero can now jump over distant crystal blocks (#42).
- The shield no longer protects while using the sword or carrying (#192).
- Languages: New syntax of strings.dat easier to read and parse (#170).
- Maps: The world property is now optional (#128).
- Maps: Teletransporters transition property is now a string (#402).
- Maps: Walls have a new property "stops_projectiles".
- Sounds: Running into a wall now plays a sound other than "explosion" (#297).

Lua API changes that introduce incompatibilities (see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)):

- Video mode names have changed: no more wide, fullscreen ou windowed names.
- sol.video.switch_mode() no longer changes the fullscreen flag.
- surface:set_transparency_color() no longer exists. Use surface:clear().
- sol.audio.play_music("none") is replaced by sol.audio.play_music(nil).
- on_key_pressed() and on_character_pressed() are now both called (#344).
- Destructible objects no longer show hardcoded dialogs (#270).
- map:create_destructible() has no hardcoded subtypes anymore (#270).
- map:create_teletransporter(): the transition is now a string (#402).
- map:create_shop_item() is replaced by map:create_shop_treasure().
- map:create_conveyor_belt() is replaced by map:create_stream() (#288).
- The state "conveyor belt" no longer exists in hero:get_state() (#288).
- The built-in strength of the sword has changed (#426).
- Bosses are not initially disabled anymore (#448).
- Call enemy:on_hurt() before enemy:on_dying() (#325).
- enemy:on_hurt() no longer takes a life_lost parameter (#426).
- The built-in defense of the tunic has changed (#428).
- enemy:get/set_magic_damage() no longer exists (#428).
- hero:start_hurt() no longer takes a magic parameter (#428).
- hero:start_hurt() now hurts the hero even when enemies cannot.
- Enemies have now a default size of 16x16 and origin of 8,13 (#354).
- The size of enemies must be a multiple of 8 (#358).
- item:on_pickable_movement_changed replaced by pickable:on_movement_changed.
- pickable:get_treasure() now returns the item instead of the item's name.
- Timers: **returning true in the callback now repeats the timer** (#152).
- sol.timer.start() now always returns the timer, even if its delay is zero.

Lua API changes that do not introduce incompatibilities:

- New Lua type and methods for custom map entities.
- New API of **destructible objects, fully customizable now** (#270).
- Colors now take an optional alpha value as fourth component.
- New functions sol.video.get/set/reset_window_size().
- New method surface:clear().
- Add **loop and callback features to sol.audio.play_music()** (#280).
- New function sol.main.get_metatable(), allows to implement **shared behaviors**.
- The lifetime of a menu can now be another menu.
- New method menu:is_started().
- Attempt to stop a menu already stopped is no longer an error.
- New method map:get_hero() (#362).
- map:get_world() can now return nil because the world is now optional (#128).
- map:create_wall() accepts a new property "stops_projectiles".
- Entity names are now auto-incremented to simplify their creation.
- New method entity:get_game() (#363).
- New methods entity:bring_to_front() and entity:bring_to_back() (#273).
- entity:test_obstacles() now takes an optional layer parameter.
- New methods enemy:get_attack_consequence(), get_attack_consequence_sprite().
- The event entity:on_created() is now called for all types of entities.
- New event enemy:on_hurt_by_sword() to customize the sword strength (#426).
- New event enemy:on_attacking_hero() to customize attacks (#356).
- New event enemy:on_hurting_hero() to customize hurting the hero (#428).
- New event hero:on_hurt() to customize the defense of the equipment (#428).
- hero:start_hurt() now takes an optional entity and sprite (#417).
- New methods hero:is/set_invincible() and hero:is/set_blinking() (#418).
- New methods hero:get/set_animation() to set custom animations (#155).
- New methods hero:get/set_sword_sound_id() to change the sword sound (#155).
- New methods to set custom tunic, sword and shield sprites (#155).
- New functions to get/set the properties of teletransporters (#403).
- New functions to get/set the properties of blocks.
- New methods to simulate game commands, by mrunderhill (#382).
- New event sensor:on_left() (#339).
- New event block:on_moving() (#334).
- New event teletransporter:on_activated() (#312).
- New event destination:on_activated() (#312).
- movement:on_position_changed() now takes x and y parameters (#342).
- Check the parameter sign in game:add/remove life/money/magic (#416).
- Check the parameter sign in item:add/remove_amount.
- New methods timer:get/set_remaining_time.
- New function sol.main.get_elapsed_time() to get the simulated time (#424).
- New function sol.input.get_key_modifiers() to get the key modifiers state.

Bug fixes:

- Fix enemy:on_hurt() that was wrongly called when immobilized.
- Fix life and money exceeding the max when the max changes (#355).
- Fix RandomMovement speed that was not taken into account (#361).
- Fix collisions detected on disabled entities (#455).
- Fix pixel collisions coordinates when sprites move (#372).
- Fix a slowness when loading maps (#374).
- Fix crash when accessing a map not active anymore (#371).
- Fix crash when changing the movement of the hero (#392).
- Fix crash when calling hero:start_treasure() with wrong parameters (#391).
- Fix crash when calling game:has/get/set_ability() with wrong name (#408).
- Fix a crash when creating two entities with the same name (#370).
- Fix issues with unobtainable treasures.
- Fix the starting location wrongly saved with special destinations (#375).
- Fix map:set_tileset() sometimes moving the hero near the map border (#400).
- Fix enemies stuck on blocks (#360).
- Fix enemies stuck on crystal blocks (#41).
- Fix human NPCs not automatically showing "walking" when moving (#336).
- Fix the hero leaving the grabbing state even while the game is suspended.
- Fix low walls in dynamic tiles behaving like normal walls.
- Fix wrong collisions of right-up and left-down diagonal jumpers.
- Fix jumpers that could be traversed sideways (#481).
- Fix blocks no longer stopping when aligned on the grid since Solarus 0.9.3.
- Fix entities not always shown when they have no optimization distance.
- Fix movement:start() raising an error if the optional callback is nil.
- Fix random_movement:get_max_radius() that was not working.
- Fix timer:is_suspended_with_map() that was not working.
- Fix crash when calling timer:set_suspended_with_map() without game started.
- Call hero:on_removed() and stop hero timers when stopping the game (#421).
- Don't die if the map or destination saved no longer exists (#301).
- Don't die if a map has no destination. Show an error and go to 0,0 instead.
- Don't die if hero:teleport() attempts to go to a non-existing place.
- Don't die if attempting to start a game without map.
- Don't die if attempting to start a non-existing dialog.

## Changes in Solarus Quest Editor 1.2

New features:

- The editor is now built with Maven (#365).
- **Multiple entities can now be resized** at the same time (#405).
- Copy-pasting entities now pastes them at the cursor (#404).
- The **map view can now be dragged** using the middle mouse button (#413).
- **Zoom in/out** in the map view using the <kbd>Ctrl</kbd> + mouse wheel (rekcah1986).
- Add buttons to edit or refresh the tileset from the map view (#425).
- The tileset view can now be dragged using the middle mouse button (#427).
- Selecting a tile now highlights its pattern in the tileset view (#290).
- Ctrl/Shift+click even on an entity now starts a selection rectangle (#410).
- **Tile patterns can now be moved in the tileset editor** (#422).
- The id and name of a new resource and now asked in a single dialog (#321).
- Show the old value in the dialog to change an id/name (#468) (rekcah1986).
- The order of resources in the quest tree can now be changed (#319).
- Increase the stepsize when scrolling the map view.
- Center the dialog of editing an entity (#443).
- Add a scroller to dialogs that are too high (#437) (rekcah1986).
- Show the map or tileset name in the "do you want to save" dialog.
- Show quest name and resource ids in the tree view (thanks rekcah1986).
- Don't place new tiles below other entities if there are some (#461).
- Switches, crystals and crystal blocks now show their actual sprite (#376).

Bug fixes:

- Fix hidden entities getting selected when clicked (#460).
- Fix selected entities losing their order when changing the layer.
- Fix NullPointerException when canceling the Open Project dialog.
- Fix freeze if tiles don't exist when changing the tileset of a map.
- Fix the tree not refreshing when deleting a resource (#335).
- Fix the tileset editor not always showing the save dialog on closing.
- Fix the num_columns property of sprites wrongly parsed.
- Fix wrong displaying of right-up and left-down diagonal jumpers.

## Incompatibilities

These improvements involve changes that introduce some incompatibilities in both the format of data files (in particular `strings.dat`) and the Lua API. Make a backup, and then see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide) on the wiki to know how to upgrade.

## Changes in Zelda Mystery of Solarus DX 1.8

New features:

- Replace fixed8.fon by a TTF font due to an SDL2 regression (#59).
- Switch fullscreen also with F11.

Bug fixes:

- Dungeon 3 5F: fix falling too early in the hole due to incorrect layers.

## Changes in Zelda Mystery of Solarus XD 1.8

New features:

- Spanish translation (beta). Thanks Xexio!
- Replace fixed8.fon by a TTF font due to an SDL2 regression (#59).
- Switch fullscreen also with F11.
- Make streams non-blocking in the crazy house.

Bug fixes:

- Fix a typo in French dialogs.
- Fix the position of some diagonal jumpers.
