---
date: '2015-08-13'
excerpt: We are proud to announce the release of a new game ! It is called Return of the Hylian - Solarus Edition. However, it is not really a new game, since...
tags:
  - solarus
title: 'New game release : Zelda Return of the Hylian (Solarus Edition)'
---

We are proud to announce the release of a new game ! It is called **_Return of the Hylian - Solarus Edition_**. However, it is not really a new game, since it is a remake of another fangame originally made by Vincent Jouillat from 2009. This time, the game has been completely remade with Solarus, thus featuring many improvements thanks to the engine.

![roth_se_logo](roth_se_logo-300x201.png)

If you've never played the original version, you will notice that the game's style and flow is rather different than our original creations : no tangled labyrinths, no hair-pulling-out enigmas, and less sinous dungeons rooms. It is a more accessible and relatively short game, but remember that _Return of the Hylian_ is actually the first iteration of a quadrology of excellent games.

This first version is not perfect, and it is possible that you still encounter some bugs. Don't hesitate to notify us, and it will be corrected as soon as possible.

What is missing in this 1.0.0 version, and will be added in the next weeks:

- Configurable controls
- English translation (for the moment, the only available language is French, but English is coming soon)
- More improvements everywhere

You can find more information and download the game on [its dedicated page](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/).

Have fun!

![link_roth_encrage_2](link_roth_encrage_2-300x300.jpg)
