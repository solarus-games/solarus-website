---
date: '2015-11-23'
excerpt: Une nouvelle version de Solarus et de nos jeux vient de sortir ! Elle corrige essentiellement de nombreux petits bugs et améliore l'utilisation de...
tags:
- solarus
title: Solarus 1.4.5 disponible, Zelda ROTH supporte les joypads
---

Une nouvelle version de Solarus et de nos jeux vient de sortir ! Elle corrige essentiellement de nombreux petits bugs et améliore l'utilisation de l'éditeur de quêtes.

Une nouveauté qui était très attendue est que [Zelda Return of the Hylian](http://www.zelda-solarus.com/zs/article/zroth-presentation/) (Solarus Edition) est maintenant jouable avec une manette ! Et vous avez la possibilité de configurer les touches :

![](large)

Par ailleurs, Zelda Mystery of Solarus DX est maintenant traduit en italien (bêta) grâce à Marco B. !

Télécharger [Solarus 1.4.5 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.4.5-win32.zip) pour Windows

- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.4.5-src.tar.gz)
- Liste complète des [changements](http://www.solarus-games.org/2015/11/22/bugfix-release-1-4-5-joypad-supported-in-zelda-roth/)
- [Blog de développement Solarus](http://www.solarus-games.org/)
- [Tutoriels vidéo](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2)
