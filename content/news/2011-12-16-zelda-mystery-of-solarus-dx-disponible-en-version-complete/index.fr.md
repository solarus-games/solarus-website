---
date: '2011-12-16'
excerpt: "Bonjour à tous, Toute l'équipe est fière de vous annoncer la sortie de Zelda : Mystery of Solarus DX, notre nouvelle création !..."
tags:
  - solarus
title: Zelda Mystery of Solarus DX disponible en version complète !
---

Bonjour à tous,

Toute l'équipe est fière de vous annoncer la sortie de Zelda : Mystery of Solarus DX, notre nouvelle création ! :)

Aujourd'hui est un grand jour dans l'histoire de Zelda Solarus.
Presque dix ans après notre premier projet, le remake version Deluxe, réalisé en C++ et Lua est enfin disponible.

- Télécharger [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-download)

Le jeu est donc disponible en version 1.0. Il est probable que fassions des mises à jour assez rapidement au fur et à mesure que nous corrigeons les inévitables bugs. Si jamais vous rencontrez des soucis, pensez à vérifier sur la page de téléchargement si une nouvelle version est disponible. N'hésitez pas à nous signaler tout problème sur le forum.

Nous espérons que vous passerez de longues heures à apprécier toutes les énigmes, les labyrinthes et les combats épiques que vous réserve le jeu. :D

Bonnes vacances à tous et joyeuses fêtes ^\_^
