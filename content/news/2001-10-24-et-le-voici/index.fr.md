---
date: '2001-10-24'
excerpt: ' Voici des screenshots exclusifs du donjon 5 ! Inutile de vous dire que la difficulté de ce niveau est haute ! Christopho innove pour le mini-boss...'
tags:
- solarus
title: Et le voici !
---

![](d5-1.jpg)
![](d5-2.jpg)
![](d5-3.jpg)

Voici des screenshots exclusifs du donjon 5 ! Inutile de vous dire que la difficulté de ce niveau est haute ! Christopho innove pour le mini-boss de ce donjon et les énigmes sont toujours aussi difficiles !

J'ai toujours pas élucidé l'énigme du mystérieux bouton gris au mur de la salle de la deuxième capture (cliquez pouragrandir) et encore plein de mystères planent dans ce niveau !

Qu'est-ce qui vous attend aussi ? De la rapidité, de l'espionnage (un peu comme sur N64), de la réflexion, de la patiente, enfin ; tout ce qui fait un zelda quoi !

Pendant les vacances, je vous montrerait aussi des screenshots exclusifs du donjon 9 qui est en préparation... ^\_^

D'ici là, je pense que chris vous fera baver avec des infos supplémentaires sur le forum où ailleurs ! N'hésitez pas à y faire un tour de temps en temps !

Netgamer ([net_gamer@hotmail.com](<http://www.zelda-solarus.com/mailto:(net_gamer@hotmail.com)>)
