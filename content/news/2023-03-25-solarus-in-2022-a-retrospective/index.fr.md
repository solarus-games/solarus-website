---
date: '2023-03-25'
excerpt: Voici un récapitulatif de ce qu'il s'est passé pour Solarus en 2022.
tags:
  - solarus
thumbnail: cover.png
title: 'Solarus en 2022: retrospective'
---

## Introduction

Comme chaque année, nous tenons l'assemblée générale de l'association Solarus Labs. Nous en profitons pour revenir sur l'année écoulée, et établir les projets à venir.

## Projets accomplis en 2022

### Ocean's Heart sur Switch

_Ocean's Heart_ est sorti sur Nintendo Switch, grâce au travail de **std::gregwar** qui a porté Solarus sur la console hybride. Le jeu de **Max Mraz** est disponible sur [le Nintendo eShop](https://www.nintendo.fr/Jeux/Jeux-a-telecharger-sur-Nintendo-Switch/Ocean-s-Heart-2160054.html), et va avoir droit à une sortie en boîte.

### Rafraîchissement de l'identité visuelle

**Olivier** a travaillé sur [l'identité visuelle du projet](/fr/about/press-kit/).

Solarus se pare d'un nouveau logo, dans la continuité du précédent, mais plus moderne.

Le site a été refait de fond en comble, pour utiliser le framework Hugo, et des déploiements automatiques.

La documentation est en cours de migration vers le nouveau site de documentation, réalisé avec MkDocs. L'ancien, généré par Doxygen, reste disponible, le temps de faire la transition.

### Organisation des dépôts Gitlab

**Hhromic** a fait le ménage et réorganisé [les dépôts sur Gitlab](https://gitlab.com/solarus-games). Les projets obsolètes ont été archivés, et les projets toujours actifs ont été classés dans des sous-groupes : games, resource-packs, etc.

De plus, les projets utilisent désormais tous le système de releases, et pour chaque release est associée un ou des paquets à télécharger, ainsi que le checksum associé. Le nom des paquets a été standardisé, et chaque release affiche son changelog.

### Projet étudiant

[IMT Altantique](https://www.imt-atlantique.fr), une école d'ingénieurs française, a demandé à ses étudiants de collaborer avec un projet open-source, dans le cadre d'un projet d'études.

Nous avons été honorés qu'un groupe d'étudiants choisisse Solarus pour leur proket. **Christopho** les a guidés dans [la résolution d'un bug du moteur](https://gitlab.com/solarus-games/solarus/-/merge_requests/1428) lors d'une soirée de pair-programming. Cela leur a permis d'apprendre les bases de la contribution à un projet open-source.

### Tutoriels vidéos

**Christopho** a continué de fournir des [tutoriels vidéo sur Youtube](https://www.youtube.com/watch?v=yVGGvOm1fsQ&list=PLzJ4jb-Y0ufzi8Qm27zkY_ncdKobEPDfV), à la fois en anglais et en français. La playlist compte aujourd'hui 61 (!) tutoriels sur l'utilisation de Solarus 1.6.

## Projets toujours en cours

Le développement de jeux continue.

- _A Link to the Dream_, mené par **Binbin**, semble sur la bonne voie pour sortir cette année.
- Le développement de _Mercuris' Chest_ a repris grâce à la motivation de **Metallizer**.

Le développement de Solarus 2.0 avance lentement mais sûrement :

- Le nouveau site vitrine utilise l'API des releases sur GitLab pour obtenir les dernières versions des jeux.

- Le développement du nouveau launcher a progressé. Le launcher va utiliser la nouvelle API du site, cité ci-dessus, pour récupérer les jeux et leurs mises à jour.

- Le nouveau site de documentation va être converti de Doxygen vers MkDocs.

- Le moteur lui-même a progressé.

## Projets au ralenti

- Le portage Android, initié par **std::gregwar**, est au ralenti depuis quelques temps. Cependant, les joueurs ont pu tester la version beta et faire part de leurs retours. Il fonctionne correctement, et ne nécessite qu'une phase de peaufinage de l'UI.

- Le portage WebAssembly, également inité par **std::gregwar**, est au ralenti. Il est bien avancé, et néceessite un peaufinage.

- Le thème Qt pour Solarus Quest Editor, initié par **Olivier**, est au point mort, faute de temps.

## Projets envisagés pour 2023

Solarus 2.0 pourrait sortir en 2023 si nous avons le temps. Cette nouvelle version du moteur comprendra des fonctionnalités multi-joueurs, un nouveau launcher, et idéalement les portages Android et WebAssembly.

_The Legend of Zelda: A Link to the Dream_, le remake Solarus de _Link's Awakening_, est en bonne voie pour une sortie en 2023.

## Conclusion

2022 a été moins productive que 2021, qui avait été exceptionnelle, mais les projets ont tout de même bien avancé. Merci à tous les membres de la communauté qui rendent ce projet possible.
