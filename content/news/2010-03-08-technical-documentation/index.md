---
date: '2010-03-08'
excerpt: Hi everyone, The blog is under construction and new pages may become available at any time. Today I am starting the technical documentation...
tags:
- solarus
title: Technical documentation
---

Hi everyone,

The blog is under construction and new pages may become available at any time. Today I am starting the [technical documentation](http://www.solarus-engine.org/solarus/) section of the engine (Solarus), which will provide a detailed specification of the engine and the quests management. Many subsections will come soon with the specification of each component of a quest (maps, tilesets, sprites, scripts, etc.). For now, you can browse the [Doxygen documentation](http://www.solarus-engine.org/doc) of the C++ code if you are interested in how the engine source code is designed.
