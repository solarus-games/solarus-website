---
date: '2008-10-19'
excerpt: "Le développement de Zelda : Mystery of Solarus DX suit son cours :). Cette semaine, j'ai développé le système de collisions au pixel près, qui..."
tags:
  - solarus
title: Les modes d'affichage
---

Le développement de Zelda : Mystery of Solarus DX suit son cours :). Cette semaine, j'ai développé le système de collisions au pixel près, qui est la base du futur système de combats. En attendant, grâce à cela, Link peut déjà couper des buissons et des herbes avec son épée.

Dans la nouvelle précédente, j'ai évoqué les modes de résolution du jeu. Je vais vous donner plus de détails à ce sujet, avec des images pour que vous ayez une idée :P.

Le jeu fonctionne à la base en mode d'affichage 320\*240 pixels. Pour information, A Link to the Past est en 256x224. L'écran de jeu est donc un peu plus grand, et c'est d'ailleurs la même taille que Mystery of Solarus et Mercuris' Chest. Il sera possible de choisir entre plusieurs modes d'affichage, en appuyant sur F5 ou bien depuis le menu des options.

Naturellement, le jeu peut s'afficher dans une fenêtre de 320x240. Mais cette fenêtre de 320\*240 étant un peu petite, si vous avez un grand écran, il faut avoir de bons yeux pour y voir quelque chose ^^. Par défaut, le jeu s'affiche donc dans une fenêtre deux fois plus grande (de taille 640x480 donc), où l'image est "étirée". Vous pouvez aussi jouer en mode plein écran. Jusqu'ici, c'est exactement comme dans Mystery of Solarus premier du nom.

En mode [640x480](http://www.zelda-solarus.com/images/zsdx/scale2x_off.png), l'image est étirée, ce qui la rend assez pixellisée. Les nostalgiques de **A Link to the Past** et de **Mystery of Solarus** s'en conteront car d'une certaine manière, c'est aussi ce qui fait le charme des anciens jeux :).

Cependant, nous vous proposons aussi une version lissée de ce mode 640x480. Il s'agit d'une technique qui permet d'arrondir les contours de l'image sans rendre l'image floue. Mais attention, le lissage reste assez discret, afin de conserver l'ambiance du jeu. Une capture d'écran en mode lissé est [disponible ici](http://www.zelda-solarus.com/images/zsdx/scale2x_on.png). Bien entendu, si vous choisissez la version lissée, vous pouvez l'afficher en plein écran ou en mode fenêtré. Vous aurez donc l'embarras du choix pour choisir le mode d'affichage qui vous conviendra le mieux ;).

Pour être complet, signalons qu'il y aura également un mode plein écran 640\*480 avec des contours noirs sur les 4 côtés, où seule la partie centrale de l'écran sera utilisée, pour afficher le jeu en taille 320x240. Ce mode, qui était disponible dans la première démo de Mercuris' Chest, permet d'obtenir un bon compromis entre la lisibilité et la pixellisation. Enfin, concernant les modes plein écran, je travaille aussi à rendre l'image bien proportionnée sur les écrans larges : les écrans larges auront donc des petites bandes noires sur les côtés en mode plein écran.

- [Voir le mode 640x480 normal (image étirée)](http://www.zelda-solarus.com/images/zsdx/scale2x_off.png)
- [Voir le mode 640\*480 lissé](http://www.zelda-solarus.com/images/zsdx/scale2x_on.png)
