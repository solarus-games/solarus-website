---
date: '2013-10-13'
excerpt: 'The new version of Solarus is available now! The main change of Solarus 1.1 is that there are no more hardcoded menus in the engine: the dialog...'
tags:
  - solarus
title: Solarus 1.1 released!
---

The new version of Solarus is available now!

The main change of Solarus 1.1 is that there are no more hardcoded menus in the engine: the dialog box and the game-over menu are now fully scripted in your quest with the Lua API. There are a lot of other new features and improvements, as well as bug fixes, so it is recommended to upgrade.

- [Download Solarus 1.1](http://www.solarus-games.org/downloads/download-solarus/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/solarus/quest-editor/ 'Solarus Quest Editor')
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html)

Our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games. ZSDX is now available in simplified Chinese and traditional Chinese (beta). Thanks Sundae and Rypervenche! Note that a Spanish version of ZSXD is also in progress (thanks Xexio!).

- [Download Zelda Mystery of Solarus DX 1.7](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.7](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/ 'Download ZSXD')

Solarus 1.1 also improves portability and performance. This allows us to release the Android and OpenPandora versions of our games. They will be available in the next few days!

## Here is the full changelog. 80 github issues were resolved for Solarus 1.1 and 20 more for ZSDX and ZSXD 1.7 so this is a huge list! Changes in Solarus 1.1

New features:

- Add a very short **sample quest** with free graphics and musics (#232, #318).
- Allow **scripted dialog boxes** (#184).
- Allow a **scripted game-over menu** (#261).
- Replace the old built-in dialog box by a very minimal one.
- Remove the old built-in game-over menu.
- Remove the old built-in dark rooms displaying (#205).
- New entity: **separators** to visually separate some regions in a map (#177).
- New type of ground: **ice** (#182).
- New type of ground: **low walls** (#117).
- Blocks and thrown items **can now fall into holes, lava and water** (#191).
- Kill **enemies that fall into holes, lava and water** (#190).
- Allow quest makers and users to set the size of the playing area.
- Allow maps to have a **default destination entity** (#231).
- A game can now start without specifying an initial map and destination.
- Stairs inside a single floor can now go from any layer to a next one (#178).
- The channel volume of `.it` musics can now be changed dynamically (#250).
- The tempo of `.it` musics can now be changed dynamically (#250).
- Allow Lua to manipulate files directly (#267).
- New syntax of **sprites**, easier to read and parse (#168).
- New syntax of `project_db.dat`, easier to read and parse (#169).
- **languages.dat** no longer exists. Languages are in project_db.dat now (#265).
- The quest archive can now also be named `data.solarus.zip` (#293).

Lua API changes that introduce incompatibilities (see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)):

- map:is_dialog_enabled() is replaced by game:is_dialog_enabled().
- map:start_dialog() is replaced by game:start_dialog().
- Remove map:draw_dialog_box(), no longer needed.
- Remove map:set_dialog_style(): replace it in your own dialog box system.
- Remove map:set_dialog_position(): replace it in your own dialog box system.
- Remove map:set_dialog_variable(): use the info param of game:start_dialog().
- Make map:get_entities() returns an iterator instead of an array (#249).
- Replace map:set_pause_enabled() by game:set_pause_allowed().
- Make the enemy:create_enemy() more like map:create_enemy() (#215).
- Remove sol.language.get_default_language(), useless and misleading (#265).
- Remove sol.main.is_debug_enabled().
- Remove map:get_light() and map:set_light() (#205).
- In game:get/set_ability(), ability "get_back_from_death" no longer exists.
- Empty chests no longer show a dialog if there is no on:empty() event (#274).

Lua API changes that do not introduce incompatibilities:

- game:get/set_starting_location(): map and destination can now be nil.
- hero:teleport(): make destination optional (maps now have a default one).
- map:create_teletransporter(): make destination optional.
- Add a function sol.video.get_quest_size().
- Make map:get_camera_position() also return the size of the visible area.
- Add a method entity:is_in_same_region(entity).
- Add a method entity:get_center_position().
- Add methods entity:get_direction4_to(), entity:get_direction8_to() (#150).
- Add a method game:get_hero().
- Add methods hero:get/set_walking_speed() (#206).
- Add hero:get_state() and hero:on_state_changed() (#207).
- Add events separator:on_activating() and separator:on_activated() (#272).
- Add methods enemy:is/set_traversable() (#147).
- Add a method enemy:immobilize() (#160).
- Add on_position_changed() to all entities, not only enemies (#298).
- Add on_obstacle_reached() to all entities, not only enemies (#298).
- Add on_movement_changed() to all entities, not only enemies (#298).
- Add on_movement_finished() to all entities, not only enemies/NPCs (#298).
- target_movement:set_target(entity) now accepts an x,y offset (#154).
- Add a method game:is_pause_allowed().
- Add a method map:get_ground() (#141).
- Add a method map:get_music() (#306).
- Add an optional parameter on_top to sol.menu.start.
- Add sprite:on_animation_changed() and sprite:on_direction_changed() (#153).
- Add a function sol.input.is_key_pressed().
- Add a function sol.input.is_joypad_button_pressed().
- Add a function sol.input.get_joypad_axis_state().
- Add a function sol.input.get_joypad_hat_direction().
- Add functions sol.input.is/set_joypad_enabled() (#175).
- Add a function sol.audio.get_music() (#146).
- Add a function sol.audio.get_music_format().
- Add a function sol.audio.get_music_num_channels().
- Add functions sol.audio.get/set_music_channel_volume() for .it files (#250).
- Add functions sol.audio.get/set_music_tempo() for .it files (#250).
- Return nil if the string is not found in sol.language.get_string().
- sol.language.get_dialog() is now implemented.
- Add a function game:stop_dialog(status) to close the scripted dialog box.
- Add an event game:on_dialog_started(dialog, info).
- Add an event game:on_dialog_finished(dialog).
- Add functions game:start_game_over() and game:stop_game_over (#261).
- Add events game:on_game_over_started(), game:on_game_over_finished (#261).
- Add sol.file functions: open(), exists(), remove(), mkdir() (#267).

Bug fixes:

- Fix map menus not receiving on_command_pressed/released() events.
- Fix camera callbacks never called when already on the target (#308).
- Fix a crash when adding a new menu during a menu:on_finished() event.
- Fix a crash when calling hero:start_victory() without sword.
- Fix an error when loading sounds (#236). Sounds were working anyway.
- Fix a possible memory error when playing sounds.
- Fix blocks that continue to follow the hero after picking a treasure (#284).
- Fix on_obtained() that was not called for non-brandished treasures (#295).
- Jumpers can no longer be activated the opposite way when in water.
- Jumpers are now activated after a slight delay (#253).
- Sensors no longer automatically reset the hero's movement (#292).
- Correctly detect the ground below the hero or any point.
- Don't die if there is a syntax error in dialogs.dat.
- Show a better error message if trying to play a Solarus 0.9 quest (#260).
- Remove built-in debug keys. This can be done from Lua now.
- Remove the preprocessor constant SOLARUS_DEBUG_KEYS.
- Call on_draw() before drawing menus.
- Fix .it musics looping when they should not.
- Log all errors in error.txt (#287).

## Changes in Solarus Quest Editor 1.1

- Add a GUI to **automatically upgrade quest files** to the latest format (#247).
- Remove the initial prompt dialog to open a quest (#264).
- Replace non-free images by **new icons** (#245).
- Add **tooltips** to the add entity toolbar.
- Simplify the add entity toolbar by showing only one icon per entity type.
- Survive when images cannot be found (#256).
- Create more content when **creating a new quest** (#258, #279).
- Improve error messages.
- Fix a crash when creating a destructible without tileset selected (#283).
- Fix the sprite field disabled in the NPC properties dialog (#303).

## Incompatibilities

These improvements involve changes that introduce some incompatibilities in both the format of data files (sprites, project_db.dat, languages.dat) and the Lua API (the dialog box and the game-over menu are scripted now!). Make a backup, and then see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide) on the wiki to know how to upgrade.

## Changes in Zelda Mystery of Solarus DX 1.7

New features:

- Add simplified Chinese and traditional Chinese translations (beta).
- Replace .spc musics by .it ones (much faster) (#17).
- Add an animated Solarus logo from Maxs (#57).

Bug fixes:

- Fix savegames created with Solarus 0.9 but that were never run.
- Fix a slight alignment issue with the hurt animation of the hero.
- Fix a small breach in dungeon 9 4F in the timed chest room (#4).
- Fix easy infinite rupees in the waterfall cave (#13).
- Fix low health beeping playing at final screen (#56).
- Add a teletransporter from south lake to the cave under the waterfall (#10).
- Dungeon 7 boss: the player could get stuck in the boss room (#6).
- Dungeon 7 boss: change the misleading hurt sound of the tail (#7).
- Dungeon 7: help the player be aligned correctly to obtain the boss key (#8).
- Dungeon 3 2F: fix two switches that disappeared when activating them.
- Dungeon 2 boss: fix tile disappearing issue (#14).
- Dungeon 9 2F: fix a minor graphical issue.

## Changes in Zelda Mystery of Solarus XD 1.7

New features:

- Replace .spc musics by .it musics (faster) and remove unused ones.
- Add an animated Solarus logo from Maxs.

Bug fixes:

- Fix savegames created with Solarus 0.9 but that were never run.
- Fix a slight alignment issue with the hurt animation of the hero.
- Dungeon 1 3F: fix "attempt to compare number with nil" in counter 36 (#3).
- Dungeon 2 1F: fix an enemy not counting in WTF room.
