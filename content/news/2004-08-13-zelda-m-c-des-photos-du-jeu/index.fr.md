---
date: '2004-08-13'
excerpt: Nous sommes en pleine période de vacances mais chez l'équipe ZS, le mot "vacances" n'a pas la même signification que pour ceux qui se dorent la...
tags:
- solarus
title: 'Zelda M''C : des photos du jeu'
---

Nous sommes en pleine période de vacances mais chez l'équipe ZS, le mot "vacances" n'a pas la même signification que pour ceux qui se dorent la pillule au soleil.

De nombreuses choses ont été intégrées au moteur du jeu ces dernières semaines, nous bénéficions d'un inventaire quasi terminé et la plupart des objets que nous inclurons dans le jeu ont été listés. Quoi ? Vous vous moquez de ce que je dis ? Les photos ? Mouais, il faut bien rassasier les paparazzi que vous êtes alors consommez ces captures avec modération parce que ce sont les premières du jeu complet.

![](develop001_mini.png)
![](develop002_mini.png)

![](develop003_mini.png)
![](develop004_mini.png)

![](develop005_mini.png)

Ce sont donc les images du moteur, il n'a aucun vrai décor, tout est pour les tests de collision, de positionnement, de vitesse, d'affichage...

Vous remarquez aussi sur la photo du menu des options que vous pourrez bien configurer vos propres touches, utiliser une manette ou un joystick, décider de jouer dans une fenêtre ou en plein écran bref, le joueur est roi !

Prochaines infos très bientôt, restés connectés !
