---
date: '2012-05-20'
excerpt: Une nouvelle version de Zelda Mystery of Solarus DX vient de sortir. Il s'agit de la version 1.5.1.
tags:
  - solarus
title: Zelda Solarus DX en espagnol
---

Une nouvelle version de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) vient de sortir. Il s'agit de la version 1.5.1.

Au programme, de nombreuses corrections et améliorations mineures, et surtout une première version bêta de la traduction en espagnol. (Notez que la traduction en anglais est disponible depuis la version 1.5.0.)

Liste des changements :

- Traduction espagnole disponible (bêta)

- Corrections diverses dans la traduction anglaise

- Tour des Cieux : le héros pouvait se retrouver dans un mur en tombant

- Tour des Cieux : améliorations mineures

- Correction d'un bug avec les flammes bleues lancées par certains boss et qui restaient bloquées

- Ancien Château : améliorations mineures

- Dédale d'Inferno : il était possible de contourner une énigme du donjon

Comme d'habitude, vous pouvez installer directement la nouvelle version à la place de l'ancienne et vos sauvegardes seront conservées.

- [Télécharger Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-download)

Merci à Clow_eriol, Emujioda, LuisCa, Musty, Xadou, Guopich et falvarez pour la traduction du jeu en espagnol. C'est un magnifique travail d'équipe. ^\_^

**Attention :** la version espagnole est en bêta-test, c'est-à-dire qu'elle nécessite d'être relue et vérifiée. Toute aide est la bienvenue pour cela : n'hésitez pas à [me contacter](http://www.zelda-solarus.com/contact.php) si vous parlez espagnol.
