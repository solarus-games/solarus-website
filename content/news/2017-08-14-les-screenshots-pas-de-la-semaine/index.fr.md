---
date: '2017-08-14'
excerpt: Cet été, toute la Solarus Team travaille d'arrache-pied sur de nombreux projets en parallèle. Zelda Mercuris' Chest est celui dont on vous parle...
tags:
- solarus
title: Les screenshots pas de la semaine
---

Cet été, toute la Solarus Team travaille d'arrache-pied sur de nombreux projets en parallèle. Zelda Mercuris' Chest est celui dont on vous parle le plus depuis un certain temps, bien que les captures d'écran se fassent plus rares ces temps-ci. Mais ce n'est pas le seul projet qui avance bien en ce moment. Il est donc temps de vous dévoiler un peu plus ce que l'on compte accomplir prochainement. Si vous suivez mes [live-streaming](https://www.youtube.com/user/ChristophoZS/live) ou mon compte Twitter [@ChristophoZS](https://twitter.com/ChristophoZS) vous savez sans doute déjà pas mal de choses !

D'ailleurs, vous l'avez peut-être remarqué, la page d'accueil de Zelda Solarus affiche désormais une section live-streaming les jours où je diffuse des sessions de créations de jeux ou de Solarus Quest Editor (merci à Binbin !).

Assez parlé, il est temps de vous en dire un peu plus sur ce qui vous attend !

### Zelda Mercuris' Chest

Le projet phare de la Solarus Team ! Newlink vous propose aujourd'hui cette capture d'écran :

![](village_midjy-300x226.png)

Il s'agit d'un village qui aura toute son importance dans le scénario ! Mais pas question de vous en dire plus.

### Zelda Oni-Link Begins (Solarus Edition)

Après [Zelda Return of the Hylian Solarus Edition](http://www.zelda-solarus.com/zs/jeu/zelda-return-of-the-hylian/), voici le remake du second jeu de Vincent Jouillat. Nous avons beaucoup avancé dessus ces dernières semaines, notamment au cours des soirées de live-streamings. On peut dire qu'il est fini à 40%. Tous les sprites des monstres, des PNJ et de Link sont faits, il reste beaucoup de mapping et un peu de scripts à faire. On avait l'espoir secret de le sortir le 12 août (date anniversaire de tous les jeux de Vincent) mais il est plus raisonnable de le terminer tranquillement sans délaisser les autres projets.

![](olb_n1-300x225.png)

### Zelda A Link to the Dream

Un petit nouveau ? Pas vraiment !

![](mabe_village-300x240.png)

Ce n'est pas un projet de la Solarus Team mais de Binbin. C'est un jeu qui avait été démarré il y a quelques années puis arrêté, mais depuis quelques semaines il renaît de ses cendres. Il est reparti pour de bon et c'est très très prometteur.

Il s'agit d'un remake de Zelda Link's Awakening avec les graphismes de Zelda A Link to the Past. La Solarus Team donne un coup de main de temps en temps ;)

Plus d'infos sur [Zeldaforce](https://zeldaforce.net/a-link-to-the-dream-un-projet-de-jeu-amateur/).

### Children of Solarus

Ici on s'éloigne un peu de Zelda Solarus car ce n'est même plus un Zelda ! Le but est de refaire [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/zs/jeu/zelda-mystery-of-solarus-dx/) mais avec des graphismes et musiques entièrement libres et sans utiliser de noms de personnages Zelda. Ça fait des années qu'on espère pouvoir y parvenir un jour, et grâce au gros travail de Diarandor sur les sprites, ça prend forme !

![](eldran-300x211.png)

Cette capture d'écran n'est pas tirée du jeu, mais d'un projet de test utilisé par Diarandor pour mettre au point les graphismes et les différents scripts. Sachez que le jeu comportera pas mal de changements par rapport à Zelda Mystery of Solarus DX. Diarandor retravaille en ce moment le tileset d'extérieur, et dès qu'il sera prêt, je commencerai à reproduire les maps de Zelda Mystery of Solarus DX avec ce tileset inédit. Vous pourrez suivre l'évènement au cours d'un futur live-streaming !

### Solarus Quest Editor

S'il y a de plus en plus de projets en cours de création avec Solarus, c'est parce que chaque version de Solarus et Solarus Quest Editor apporte son lot de nouveautés et devient de plus en plus puissante et facile à utiliser. Alors nous travaillons déjà sur la prochaine version (Solarus 1.6), dont la principale nouveauté sera une fonctionnalité que les mappeurs attendent tous : les autotiles ! Les autotiles permettent en fait de générer automatiquement des tiles de bordure autour d'un chemin par exemple, ou autour d'un sol de donjon. Les utilisateurs de RPG Maker connaissent bien cela. Le développement de ce nouvel outil est donc en cours, et je peux vous dire que ça fonctionne déjà très bien. Avec les autotiles, vous pourrez générer des bordures de taille au choix, ce qui permettra de couvrir énormément de cas d'utilisation. Le but, c'est que l'on puisse tracer des donjons très rapidement : on placerait juste le sol, et les bordures correspondantes mais aussi les murs seraient ajoutés automatiquement.

![](autotiles-300x202.png)

Le rêve de tout mappeur :)
