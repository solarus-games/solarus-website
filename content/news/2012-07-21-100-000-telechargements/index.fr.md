---
date: '2012-07-21'
excerpt: Une petite news simplement pour vous remercier d'être aussi passionnés par nos jeux.
tags:
  - solarus
title: 100 000 téléchargements !
---

Une petite news simplement pour vous remercier d'être aussi passionnés par nos jeux. [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-download) a en effet dépassé il y a quelques jours la barre des 100 000 téléchargements !

![](http://www.zelda-solarus.com/images/zsdx/dungeon_8_holes.png)

Si nos créations vous plaisent, continuez à en parler autour de vous. ^\_^

Encore merci à tous. L'épopée continue?
