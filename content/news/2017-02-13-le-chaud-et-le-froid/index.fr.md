---
date: '2017-02-13'
excerpt: La capture d'écran de la semaine de Zelda Mercuris' Chest vous emmène dans des montagnes enneigées mais où la lave n'est pourtant jamais bien...
tags:
- solarus
title: Le chaud et le froid
---

La capture d'écran de la semaine de Zelda Mercuris' Chest vous emmène dans des montagnes enneigées mais où la lave n'est pourtant jamais bien loin.

![volcanoscreen](volcanoscreen-300x226.png)

Bonne semaine à toutes et à tous.
