---
date: '2017-01-23'
excerpt: La capture d'écran de Zelda Mercuris Chest du jour est un temple majestueux dans lequel Link va devoir rechercher quelque chose de...
tags:
- solarus
title: Un temple mystérieux
---

La capture d'écran de Zelda Mercuris Chest du jour est un temple majestueux dans lequel Link va devoir rechercher quelque chose de précieux

![outside_crater_temple](outside_crater_temple-300x225.png)

On ne peut pas vous en dire plus, mais encore une fois c'est à Newlink que vous devez envoyer les félicitations pour ces graphismes.
