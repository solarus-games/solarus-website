---
date: '2004-11-04'
excerpt: Alors que @PyroNet, Seb le Grand et Geomaster travaillent d'arrache-pied pour maintenir le site à jour, je travaille toujours de mon côté sur le...
tags:
- solarus
title: Des nouvelles de Zelda M'C
---

Alors que @PyroNet, Seb le Grand et Geomaster travaillent d'arrache-pied pour maintenir le site à jour, je travaille toujours de mon côté sur le projet Zelda M'C. Bien que je ne donne pas beaucoup de nouvelles, le développement a repris et le moteur de jeu a avancé depuis la [dernière mise à jour](http://www.zelda-solarus.com/index.php?id=365).

Quelques nouveaux éléments du moteur ont été programmés. Certains d'entre eux ont été repris de la démo et améliorés, d'autres sont nouveaux, comme par exemple certaines attaques inédites... Voici d'ailleurs une toute nouvelle image de la version de développement :

![](develop006.png)

[Voir les autres images](http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=scr)

Ce que l'on peut dire à l'heure actuelle, c'est que beaucoup de choses sont déjà faites, et qu'il en reste encore beaucoup à faire :-)
