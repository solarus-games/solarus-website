---
date: '2013-10-14'
excerpt: ' Une nouvelle version du moteur Solarus vient de sortir, de même qu''une mise à jour de Zelda Mystery of Solarus DX et de Zelda Mystery of...'
tags:
- solarus
title: Solarus 1.1 disponible
---

![Logo du moteur Solarus](solarus-logo-black-on-transparent-300x90.png)

Une nouvelle version du moteur Solarus vient de sortir, de même qu'une mise à jour de Zelda Mystery of Solarus DX et de Zelda Mystery of Solarus XD.

Si vous suivez notre [tutoriel vidéo](http://www.youtube.com/watch?v=9n-aottUQUA&list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2) ou [écrit](http://wiki.solarus-games.org/doku.php?id=fr:tutorial:create_your_2d_game_with_solarus), vous savez déjà que de nombreuses améliorations étaient promises pour Solarus 1.1. Cette nouvelle version est désormais disponible en téléchargement !

Il y a énormément de nouveautés dans le moteur, qui vous intéresseront si vous l'utilisez pour développer votre propre projet de jeu. Parmi les plus importantes, un nouveau type d'entité est disponible : les séparateurs, qui vous permettent de séparer visuellement une map en plusieurs régions. Deux nouvelles sortes de terrain sont proposées : la glace et les murets. Le format de certains fichiers de données a changé, en particulier celui des sprites qui est maintenant beaucoup plus lisible. Autre point essentiel : chaque jeu peut maintenant personnaliser sa boîte de dialoge et son menu de game-over. Les ennemis et les blocs peuvent maintenant tomber dans les trous et la lave. Solarus 1.1 apporte aussi son lot de corrections de bugs.

- Télécharger [Solarus 1.1 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.1.0-win32.zip) pour Windows
- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.1.0-src.tar.gz)
- Liste complète des [changements](http://www.solarus-games.org/2013/10/13/solarus-1-1-released/)
- [Blog de développement Solarus](http://www.solarus-games.org/)
- Comment [convertir votre quête](http://wiki.solarus-games.org/doku.php?id=fr:migration_guide) de Solarus 1.0 vers Solarus 1.1

Du côté de nos créations, elles ont été mises à jour pour profiter de ces améliorations et corrections de bugs. En particulier, vous pouvez maintenant enfin pousser les ennemis dans les trous et la lave :)

- Télécharger [Zelda Mystery of Solarus DX 1.7](http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/)
- Télécharger [Zelda Mystery of Solarus XD 1.7](http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/)

Ce n'est pas tout puisque Zelda Mystery of Solarus DX 1.7 est maintenant traduit en chinois simplifié et chinois traditionnel (merci Sundae et Rypervenche !). Zelda Mystery of Solarus XD est quant à lui en cours de traduction en espagnol (merci Xexio !). Une autre amélioration visible est que le logo Solarus qui apparaît au démarrage de nos jeux, réalisé par Neovyse, est maintenant animé grâce au travail de Maxs (merci !).

Autre nouveauté très très attendue : cette version sera d'ici quelques heures officiellement disponible sur Android grâce au portage réalisé par Sam101 (merci aussi !). Enfin, sachez qu'elle fonctionne également sur les consoles open-source [OpenPandora](http://openpandora.org/) et [GCW-Zero](http://www.gcw-zero.com/).

N'hésitez pas à nous signaler tout problème si par hasard un bug nous aurait échappé ! En tout cas, j'espère que vous apprécierez ces améliorations. Pendant ce temps, de notre côté, nous continuons le travail sur Mercuris' Chest et sur Solarus 1.2.
