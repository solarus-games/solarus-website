---
date: '2012-04-27'
excerpt: '10 ans déjà ! Le 26 avril 2002, sortait notre première création, Zelda Mystery of Solarus.'
tags:
  - solarus
title: 10 ans !
---

**10 ans déjà !**

Le 26 avril 2002, sortait notre première création, Zelda Mystery of Solarus.

![](http://www.zelda-solarus.com/images/zs/solarus-titre.jpg)

Réalisé avec RPG Maker 2000, c'était l'un des premiers Zelda amateurs complets. Son développement a duré un an et 4 mois, une belle prouesse. Il est vrai que le moteur jeu était basique... et que j'avais peut-être plus de temps libre qu'aujourd'hui :)

En 10 ans, le jeu a été téléchargé 640 000 fois. S'il est aujourd'hui dépassé par son successeur [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx), il aura marqué marqué l'histoire des Zelda amateurs.
Merci à tous pour votre fidélité et votre passion. ^\_^

Bon anniversaire ZS !

![](http://www.zelda-solarus.com/images/uploads/animpubzs.gif)
