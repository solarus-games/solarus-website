---
title: Donate
excerpt: How to donate to the Solarus game engine project.
tags: [source code, donation, donate]
aliases:
  - /donation
  - /about/donation
  - /development/donation
  - /en/development/donation
---

## Why donate

All the software made for the Solarus engine project is free and open-source, and will be forever: the engine, the quest editor, the quest launcher, the website, the resources.

We spend a lot of time working on the project, and we intend to continue!

## Where the money goes

Money will go to Solarus Labs, the nonprofit organization that promotes Solarus, and will be totally reinvested into the project.

Here is, for instance, how the donations will be used:

- Hiring developers or artists to work on the project,
- Paying the server hosting fees,
- Purchasing required stuff to develop Solarus (hardware, software),
- Covering travel costs for meetings, conferences and other events,
- Producing Solarus merchandising (autocollants, t-shirts, posters, etc.).

## How to donate

You can donate any amount you want via the following ways. Please [contact us](/about/contact) if you want to donate via a different way. Your support is very appreciated!

{{< donation >}}
