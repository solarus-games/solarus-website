---
title: Faire un don
excerpt: Comment faire un don au projet de moteur de jeu Solarus.
tags: [donation, donate, don]
aliases:
  - /fr/donation
  - /fr/don
  - /fr/about/donation
  - /fr/about/don
  - /fr/development/donation
---

## Pourquoi faire un don

Tous les logiciels créés dans le cadre du projet de moteur de jeu Solarus sont [libres et open-source](https://gitlab.com/solarus-games), et le seront pour toujours: le moteur, l'éditeur de quêtes, le lanceur de quêtes, le site Web, les ressources, et les jeux que nous avons faits.

Nous passons beaucoup de temps sur ces projets, et nous n'avons pas l'intention d'arrêter !

## Où va l'argent

L'argent sera reversé à Solarus Labs, l'association qui soutient Solarus, et sera totalement réinvesti dans le projet.

Voici à quoi peuvent par exemple servir les dons :

- Payer des développeurs ou des artistes afin qu'ils travaillent sur le projet
- Payer les coûts d'hébergement du site
- Acheter le matériel nécessaire pour déveloper Solarus (machines, logiciels)
- Couvrir les frais de transport pour les réunions, conférences ou autres évènements
- Créer du merchandising Solarus (autocollants, t-shirts, posters, etc.)

## Comment faire un don

Vous pouvez faire un don du montant que vous voulez via les moyens suivants. Veuillez nous [contacter](/fr/about/contact) si vous désirez faire un don avec un moyen autre que les suivants. Sachez que votre support fait extrêmement plaisir !

{{< donation >}}
