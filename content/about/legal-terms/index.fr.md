---
title: Informations légales
excerpt: Informations légales à propos du projet de moteur de jeu Solarus (entité légale, licences).
tags: [legal, law, ip, intellectual property, licensing, license, npo, non profit, nonprofit, organization, association]
aliases:
  - /fr/legal
  - /fr/nonprofit-organization
  - /fr/about/nonprofit-organization
---

## Entité légale de Solarus Labs

![Solarus Labs logo](solarus-labs-logo.png)

**Solarus Labs** est une **association française loi 1901 à but non lucratif**, créée en mars 2021. Le but de l'association est de développer le moteur de jeu Solarus, de le promouvoir auprès du public, ainsi qu'éduquer à la création de jeu vidéo avec des logiciels libres.

Solarus Labs, en tant qu'organisme, est une personne légale ayant sa propre capacité juridique, et sa propre responsabilité, distincte des personnes qui la dirigent.

Le bénévolat est la règle concernant l'association : tous les dons et recettes sont conservées et utilisées par la personne morale, c'est-à-dire l'association, sans aucune distribution aux dirigeants ou aux membres, malgré leur travail et leur investissement.

Tous les dons iront directement à l'association, et seront réinvesties dans le projet.

### Documents

La plupart des documents administratifs sont publics et accessibles dans le [dépôt Gitlab de l'association](https://gitlab.com/solarus-games/solarus-labs).

Il y a une assemblée générale annuelle durant laquelle les membres de l'association se réunissent et font le bilan des finances et du développement de l'association durant l'année écoulée.

### Bureau

Le bureau actuel de Solarus Labs est composé de :

- **Président:** Christophe Thiéry
- **Trésorier:** Kévin Baumann
- **Vice-Trésorier:** Benjamin Schweitzer
- **Secrétaire:** Olivier Cléro

## Droit applicable

L'association Solarus Labs est une association de droit français. Elle est domiciliée en France et exerce l'essentiel de la gestion de son activité dans ce pays. La loi applicable la concernant (ainsi que ses logiciels et services) est donc dans tous les cas la loi française, comme prévu par les articles 14 et 15 du Code civil.

![French Government logo](french_gouv_logo.png)

## Licences et droit d'auteur

### Logiciels

Les logiciels proposés par Solarus Labs sont tous des logiciels libres et open source, sous la licence [GNU GPL v3](https://www.gnu.org/licenses/quick-guide-gplv3.html). L'ensemble des codes source des logiciels est accessible sur [Gitlab](https://gitlab.com/solarus-games).

Cette licence autorise toutes les utilisations des logiciels et leur redistribution sous la même forme ou après modification, à condition que ces logiciels modifiés respectent les conditions de la licence d'origine, notamment la publication du code source et l'autorisation de redistribution.

![GNU GPL v3 logo](gpl_v3_logo.png)

### Ressources

Les ressources produites par Solarus Labs sont toutes libres, sous licence [Creative Commons Attribution-ShareAlike 4.0 International 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Les ressources incluent tout type de contenu produits dans le cadre du projet Solarus: images, sons, textes, contenu de ce site Internet, etc.

Ce qui ne signifie pas que ces ressources sont dans le domaine public pour autant. Cette licence autorise l'utilisation, le partage et la modification, à condition que les ressouces modifiées respectent les conditions de la licence d'origine, notamment le crédit de l'auteur de l'œuvre originelle.

![CC-BY-SA](cc_by_sa.png)

### Propriété intellectuelle

Subsidiairement aux conditions de la license, le droit commun français de la propriété intellectuelle s'applique, notamment le droit d'auteur. Tout changement de licence ou redistribution sous d'autres conditions est interdite. De même, la provenance des logiciels, même partiellement réutilisés, doit toujours être visible par les utilisateurs.

## Brevets

Selon l'article `L 611-10` du Code de la propriété intellectuelle, les brevets logiciels ne sont pas admis en droit français. Au niveau communautaire, l'article `52` de la Convention sur le brevet européen a également exclu la protection des programmes d'ordinateur.

Par conséquent, les logiciels fournis par Solarus Labs ne sont pas redevables à des licences sur n'importe quel brevet logiciel, quelle qu'en soit sa provenance.

## Responsabilité d'utilisation des logiciels

L'association Solarus Labs décline toute responsabilité quand à une utilisation illégale de ses logiciels.
