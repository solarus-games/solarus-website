---
title: Solarus Launcher
downloadPackage: solarus-launcher
i18nKeys:
  description: downloadLauncherDesc
  help: downloadLauncherHowTo
  button: downloadLauncherUserManual
# docsEndpoint: launcher # Uncomment when launcher doc is ready.
type: lists
layout: download-package
redirect: /download/solarus-launcher/index.json
---
