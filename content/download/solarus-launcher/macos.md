---
title: macOS
downloadPackage: solarus-launcher
platform: macos
gitlabProjectId: 6933824
gitlabAssetFileRegex: "solarus-player-v?\\d+.\\d+.\\d+-macosx64.zip"
version: 1.6.4
type: singles
layout: download-package-platform
redirect: /download/solarus-launcher/macos/index.json
---
