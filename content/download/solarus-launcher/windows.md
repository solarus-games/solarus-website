---
title: Windows
downloadPackage: solarus-launcher
platform: windows
gitlabProjectId: 6933824
gitlabAssetFileRegex: "solarus-player-x64-v\\d+.\\d+.\\d+.zip"
version: latest
type: singles
layout: download-package-platform
redirect: /download/solarus-launcher/windows/index.json
---
