---
title: Anciens téléchargements
excerpt: Liens de téléchargement pour les versions précédentes du moteur Solarus et de ses outils de création de jeux.
tags: [previous, précédent]
type: singles
layout: previous-downloads
aliases:
  - /fr/download/old
  - /fr/download/previous
---

Veuillez trouver ici les liens de téléchargement pour les versions précédentes du moteur Solarus et de ses outils de création de jeux.
