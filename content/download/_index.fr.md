---
title: Téléchargement
excerpt: Téléchargez le moteur de jeu Solarus et ses outils d'aide à la création.
type: singles
layout: download
tags: [download, install, installer, dmg, executable, engine, player, windows, macos, mac, linux, editor, ide, télécharger, téléchargement, installeur]
aliases:
  - /fr/engine
  - /fr/downloads
  - /fr/telechargement
  - /fr/telecharger
---

Selon que vous soyez un [joueur](#solarus-launcher) ou un [développeur de jeux](#solarus-quest-editor), voici les paquets à télécharger.
