---
title: Linux
downloadPackage: solarus-quest-editor
platform: linux
gitlabProjectId: 10101554
gitlabAssetFileRegex: "solarus_\\d+.\\d+.\\d+_amd64.snap"
version: latest
type: singles
layout: download-package-platform
redirect: /download/solarus-quest-editor/linux/index.json
---
