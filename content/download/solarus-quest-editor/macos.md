---
title: macOS
downloadPackage: solarus-quest-editor
platform: macos
gitlabProjectId: 6933848
gitlabAssetFileRegex: "solarus-v?\\d+.\\d+.\\d+-macosx64.zip"
version: 1.6.4
type: singles
layout: download-package-platform
redirect: /download/solarus-quest-editor/macos/index.json
---
