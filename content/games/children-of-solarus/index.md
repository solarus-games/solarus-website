---
age: all
controls:
  - keyboard
  - gamepad
developer: Solarus Team
excerpt: 'A 100% free and open-source remake of The Legend of Zelda: Mystery of Solarus DX (which was already a remake).'
genre:
  - Action-RPG
  - Adventure
id: children-of-solarus
languages:
  - en
  - fr
  - es
licenses:
  - GPL v3
  - CC-BY-SA 4.0
maximumPlayers: 1
minimumPlayers: 1
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/children-of-solarus
thumbnail: thumbnail.png
title: Children of Solarus
website: https://www.solarus-games.org/games/children-of-solarus
---

_Children of Solarus_ is the 100% free remake of _The Legend of Zelda: Mystery of Solarus DX_. All proprietary content is replaced by 100% libre content: custom sprites, tilesets, musics, sounds, and so on! Story and characters are a bit expanded to make the game stand on its own, instead of using _Zelda_ lore.

controls: ['touch', 'mouse', 'keyboard', 'gamepad']
