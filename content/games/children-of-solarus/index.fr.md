---
excerpt: 'Un remake 100% libre et open-source de The Legend of Zelda: Mystery of Solarus DX (qui était déjà un remake).'
---

_Children of Solarus_ est le remake 100% libre de _The Legend of Zelda: Mystery of Solarus DX_. Tout le contenu propriétaire est remplacé par du contenu 100% libre : sprites originaux, tilesets originaux, musiques originales, sons orginaux, etc. L'histoire et les personnages sont un peu étendus afin de permettre au jeu de se suffire à lui-même, au lieu de se baser sur la série _Zelda_.
