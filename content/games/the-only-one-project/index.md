---
age: all
controls:
  - keyboard
  - gamepad
developer: KaKaShUruKioRa
download: KaKaShUruKioRa/The-Only-One-Project
downloadType: github_api
excerpt: Dive into a short but intense Zelda adventure featuring a sprawling dungeon packed with challenging puzzles and bosses.
genre:
  - Action-RPG
  - Adventure
id: the-only-one
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2021-01-09
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
solarusVersion: 1.6.x
sourceCode: https://github.com/KaKaShUruKioRa/The-Only-One-Project
thumbnail: thumbnail.png
title: The Only One
version: 0.1.3
walkthroughs:
  - type: video
    url: https://www.youtube.com/playlist?list=PLMZ5rozT58YaugIRivT_ml9XCzewiJId-
website: https://www.solarus-games.org/games/the-only-one-project
---

### Presentation

**The Only One Project** is a small hardcore adventure with a pre-dungeon, and a big dungeon with lots of twisted puzzles, a "funny" mini-boss, and a big final boss. The game reuses some classic mechanics from _The Legend of Zelda: A Link to the Past_. You will encounter a few other unique systems that will require you to think according to the environment. To assist you in your quest, you can ask for hints from the tablets in the dungeon.

### Synopsis

Link awakens in his home, but something is amiss—he’s in a parallel world reminiscent of A Link to the Past, yet he’s unarmed and unprepared! Determined to uncover the truth, he ventures outside in search of answers. Soon, he learns that a dense, dark haze has engulfed the land, and strange occurrences are unfolding at a nearby castle. With the guards barring entry, Link must devise a clever plan to bypass their watch and infiltrate the castle, unraveling the mysteries that lie within...
