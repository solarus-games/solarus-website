---
excerpt: Plongez dans une aventure Zelda courte mais intense avec un donjon tentaculaire rempli d'énigmes et de boss difficiles.
---

### Présentation

**The Only One Project** est une petite aventure hardcore avec un pré-donjon, et un gros donjon avec plein d'énigmes tordues, un mini-boss "rigolo", et un gros boss final et original dans son pattern. Le jeu réutilise quelques mécaniques classiques de _The Legend of Zelda: A Link to the Past_. Vous y trouverez quelques autres systèmes uniques, qui vous demanderont de réfléchir en fonction de l'environnement. Pour vous aider dans votre quête, vous pourrez demander des indices aux stèles dans le donjon.

### Synopsis

Link se réveille chez lui, dans un monde parallèle à celui de "A Link to the Past", mais cette fois-ci, il se retrouve sans arme ni équipement ! Désorienté, il décide de sortir pour chercher des réponses et comprendre ce qui se passe. Il découvre rapidement qu'une épaisse brume sombre enveloppe le monde, et que d'étranges événements semblent se dérouler dans un château voisin. Cependant, les gardes en interdisent l'accès, obligeant Link à trouver un autre moyen de s’infiltrer et de percer les mystères qui l’entourent...
