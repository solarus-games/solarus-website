---
age: all
controls:
  - keyboard
  - gamepad
developer: KaKaShUruKioRa
download: KaKaShUruKioRa/A-Link-to-the-Past-Project
downloadType: github_api
excerpt: 'A Solarus-made remake of The Legend of Zelda: A Link to the Past.'
genre:
  - Action-RPG
  - Adventure
id: alttp
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
solarusVersion: 1.6.x
sourceCode: https://github.com/KaKaShUruKioRa/A-Link-to-the-Past-Project
thumbnail: thumbnail.png
title: 'The Legend of Zelda: A Link to the Past'
version: 0.4.1
website: https://www.solarus-games.org/games/the-legend-of-zelda-a-link-to-the-past
---

### Presentation

This Solarus-made remake of the 1992 Super Nintendo (SNES) cult classic _The Legend of Zelda: A Link to the Past Project_ uses the graphical elements from the European (PAL) version. A few differences aside, mainly due to the modernity of the Solarus Engine, it will look and feel like the original from yesteryear.

![Link](artwork_triforce.jpg 'Triforce in the Sacred Kingdom')

### Synopsis

**Link** awakens abruptly in his home, stirred by a telepathic plea for help from **Zelda**, the Princess of the Hyrule Kingdom. Amidst a raging storm, his uncle ventures outside, armed with a sword and shield, determined to reach Hyrule Castle. Choosing not to stay behind, Link resolves to follow him and answer Princess Zelda's call for help.

As you step into the role of Link, you will embark on an epic journey to rescue Princess Zelda and protect the Kingdom of Hyrule from the looming threat of Dark Forces!

![Link](artwork_mastersword.jpg 'Mastersword')
