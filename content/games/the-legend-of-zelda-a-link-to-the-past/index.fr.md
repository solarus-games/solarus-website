---
excerpt: 'Un remake Solarus de The Legend of Zelda: A Link to the Past.'
---

### Présentation

Ce remake réalisé par Solarus du classique culte de la Super Nintendo (SNES) de 1992 _The Legend of Zelda: A Link to the Past Project_ utilise les éléments graphiques de la version européenne (PAL). Mis à part quelques différences, principalement dues à la modernité du moteur Solarus, il ressemblera trait pour trait au jeu d'antan.

![Link](artwork_triforce.jpg 'La Triforce et le Royaume Sacré')

### Synopsis

**Link** se réveille brusquement chez lui, alerté par un appel à l'aide télépathique de **Zelda**, la Princesse du Royaume d'Hyrule. Bravant une violente tempête, son oncle s'aventure à l'extérieur, armé d'une épée et d'un bouclier, déterminé à rejoindre le Château d'Hyrule. Choisissant de ne pas rester en arrière, Link décide de le suivre et de répondre à l'appel de la princesse Zelda.

En endossant le rôle de Link, vous vous embarquerez dans une quête épique pour sauver la Princesse Zelda et protéger le Royaume d'Hyrule de la menace grandissante des Forces Obscures !

![Link](artwork_mastersword.jpg "L'épée de Légende")
