---
excerpt: La suite du premier jeu de la série parodique Zelda XD. Plus grand. Plus fort. Plus débile.
---

### Présentation

_The Legend of Zelda XD2: Mercuris Chess_ est la suite du premier jeu parodique de la série _XD_, et a lui aussi été publié un 1er avril, en 2017 cette fois-ci. En tant que suite directe, le monde est repris et étendu, l'histoire est prolongée et le ton humoristique est conservé, si ce n'est amélioré !

Le jeu a été fait en 10 semaines seulement, en partant d'une discussion sur la passion dévorante de Christopho pour le jeu d'échecs. Diarandor a suggéré qu'un jeu pourrait alors être appelé _Mercuris Chess_, en référence au projet _Mercuris' Chest_, et le même jour il fut décidé que ce jeu serait le prochain poisson d'avril de l'équipe.

L'équipe a travaillé très dur, et vous pourrez constater les améliorations par rapport aux précédents jeux. Le jeu comporte deux grands donjons, de nombreuses quêtes secrètes et d'innombrables références, et des personnages totalement absurdes au quatre coins du royaume. Assurez-vous de parler à tout le monde pour ne manquer aucune blague ! Mais attention : parmi ces discussions qui sont souvent sans queue ni tête, se cachent parfois certains indices très utiles pour débloquer des trésors qui semblaient inaccessibles dans le premier épisode…

![Link](artwork_link.png)

### Synopsis

L'histoire se situe quelques mois après la nuit terrible des évènements du premier jeu _XD_. Un richissime homme d'affaires nommé M. Grump a pris le contrôle du royaume et a racheté tout ce qu'il pouvait. Alors que Link essaye de se faire pardonner auprès de la Princesse Zelda, un énorme incendie dans la forêt semble réjouir tout le village…

C'est ainsi que commence votre aventure, dans un monde où tout a un prix et où la vérité et le mensonge sont devenus difficiles à distinguer.

![M. Grump](artwork_grump.png)
