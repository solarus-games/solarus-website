---
excerpt: La suite d'A Link To The Past, et le premier jeu fait avec Solarus.
---

### La suite d'_A Link To The Past_

**_The Legend of Zelda: Mystery of Solarus DX_** se positionne comme étant la suite directe de _The Legend of Zelda: A Link to the Past_ sur SNES, utilisant les mêmes graphismes et mécaniques de jeu. _Mystery of Solarus DX_ fut le premier jeu fait avec Solarus, et en réalité, Solarus a été conçu principalement pour ce jeu.

![Link](artwork_link.png)

### Origines

_Mystery of Solarus DX_ est en réalité une refonte d'une première version, _Mystery of Solarus_ (sans le suffixe _DX_). Cette première création, développé avec RPG Maker 2000, fut publiée en 2002 et était uniquement disponible en français. Le projet _DX_ a été dévoilé le 1er avril 2008. Ses objectifs étaient de corriger les nombreux défauts de son prédecesseur : le système de combat, les boss, l'utilisation des objets, etc.

Cependant, ce n'est pas tout : de nouveaux éléments graphiques et de nouvelles musiques ont été créées pour vous accompagner durant le jeu. Cette version _Deluxe_ est l'opportunité de revivre l'aventure originale d'une toute nouvelle façon, ou de la découvrir pour la première fois si vous n'y aviez jamais joué avant !

![Ganon](artwork_ganon.png)

### Synopsis

Après les évènements de _A Link to the Past_, Ganon a été vaincu et emprisonné pour toujours dans la Terre d'Or par le roi d'Hyrule, grâce au sceau des Sept Sages.

Les années passèrent, et un jour le Roi fut atteint d'une étrange et grave maladie. Les docteurs et apothicaires de tout le royaume sont venus essayer de le soigner, mais en vain. Le roi ne survécut pas, et son pouvoir se volatilisa, fragilisant alors le sceau des Sept Sages.

Le héros, sur les conseils de son maître Sahasrahla, fit confiance à l'héritière du trône : la princesse Zelda. Zelda, avec la Triforce, s'associa à huit mystérieux enfants pour éparpiller la Triforce en huit fragments de par le monde. La paix était alors maintenue dans le royaume… jusqu'au jour où débute votre épopée.

Découvrez une grande aventure pleine de donjons, de monstres, de personnages mystérieux, de labyrinthes et d'énigmes.

![Princesse Zelda](artwork_zelda.png)
