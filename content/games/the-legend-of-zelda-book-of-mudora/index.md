---
age: all
controls:
  - keyboard
  - gamepad
developer: Matt Wright
download: https://www.dropbox.com/s/5fz908ikkkjzat1/zbom.solarus?dl=1
downloadType: solarus
excerpt: Stop the Dark Tribe from destroying Hyrule by obtaining the power of the Book of Mudora.
genre:
  - Action-RPG
  - Adventure
id: zbom
languages:
  - en
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2017-08-15
latestUpdateDate: 2019-05-29
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x
sourceCode: https://github.com/wrightmat/zbom
thumbnail: thumbnail.png
title: 'The Legend of Zelda: Book of Mudora'
version: 1.4
website: https://wrightmatta.wixsite.com/zeldabom
---

Link's newest quest is set a few generations after Twilight Princess and spans two different continents of Hyrule. Explore eight unique temples and defeat enemies and bosses in order to gain powerful items to assist you in your goal. Explore many areas of Hyrule and assist different races in their conflicts.
