---
excerpt: Un concept plus qu'un jeu, the Labors of Zeldo vous propose de visiter, tel un musée, tous les travaux que le Créateur Zeldo n'a jamais sorti de manière officielle.
---

### Système de jeu

Dans _The Labors of Zeldo_, vous pouvez explorer les projets inédits réalisés par ZeldoRetro à l'aide du moteur Solarus.

Jeux, retranscriptions de contenus existants ou simples tests, ZeldoREtro vous propose d'y jouer dans une version totalement jouable et agréable.

### Vague de contenus 1 - Tower of the Triforce

Cette 1ère vague de contenu concerne _The Legend of Zelda : Tower of the Triforce_, un projet réalisé entre 2017 et 2019 et jamais terminé, par manque d'inspiration, trop d'idées et trop d'ambition.

La vague dispose de plusieurs environnements avec de nombreux objets à collectionner pour allonger le temps de jeu, et de nombreuses notes de développement, astuces et anecdotes pour récompenser les joueurs qui les collectionnent tous. Sa durée est estimée entre 4 et 6 heures.
