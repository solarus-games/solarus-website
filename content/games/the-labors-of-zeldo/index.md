---
age: all
controls:
  - keyboard
  - gamepad
developer: ZeldoRetro
download: ZeldoRetro/labors_zeldo
downloadType: github_api
excerpt: A concept more than a game, the Labors of Zeldo offers you the opportunity to explore, like a museum, all the works that the Creator Zeldo never officially released.
genre:
  - Action-RPG
  - Adventure
id: labors_zeldo
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2024-01-30
latestUpdateDate: 2024-02-29
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
  - screen9.png
  - screen10.png
  - screen11.png
  - screen12.png
solarusVersion: 1.6.x
sourceCode: https://github.com/ZeldoRetro/labors_zeldo
thumbnail: thumbnail.png
title: The Labors of Zeldo
version: 1.1.0
website: https://www.solarus-games.org/games/the-labors-of-zeldo
---

### Gaming system

In _The Labors of Zeldo_, you can explore the unreleased projects that ZeldoRetro made using the Solarus Engine.

Games, retranscriptions of existing contents or simple tests, ZeldoREtro is offering you to play them in a totally playable and enjoyable version.

### Wave 1 - Tower of the Triforce

This 1st Wave of content is about _The Legend of Zelda: Tower of the Triforce_, a project made during 2017-2019 and never finished, due to a lack of inspiration, too many ideas and too much ambition.

The wave have several environments with many collectibles to lengthen the playtime, and a lot of development notes, tips and anecdotes to reward the players who collects them all. It is estimated to have a length of 4 to 6 hours.
