---
title: Vincent Jouillat
excerpt: The author of the Zelda fangame trilogy that includes Return of the Hylian, Oni-Link Begins et Time to Triumph. He made his own game engine, and some of his game have been remade with Solarus.
thumbnail: thumbnail.png
website: http://www.zeldaroth.fr
---
