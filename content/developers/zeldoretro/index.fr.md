---
excerpt: Créateur fantasque. Auteur de la série des Défis de Zeldo. Passionné de Zelda, il développe depuis 2015 "Forgotten Legend", un fangame Zelda qui sera son projet de vie ultime.
---
