---
title: ZeldoRetro
excerpt: Author of the Zeldo Challenges series. Passionated about Zelda, since 2015 he has been developing "Forgotten Legend", a Zelda fangame which will be his ultimate project.
thumbnail: thumbnail.png
website: https://www.youtube.com/@zeldogames7340
---
