---
title: Features
excerpt: Discover the Solarus game engine features.
type: singles
layout: features
tags: [features, presentation]
---

Check the video below to have an overview of what Solarus is capable of!
